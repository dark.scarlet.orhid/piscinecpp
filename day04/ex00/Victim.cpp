#include "Victim.hpp"
#include <iostream>

Victim::Victim() { return; }

Victim::Victim(std::string name)
{
	this->_name = name;
	std::cout << "Some random victim called "
			  << this->_name
			  << " just popped !"
			  << std::endl;
	return;
}

Victim::~Victim()
{
	std::cout << "Victim "
			  << this->_name
			  << " just died for no apparent reason !"
			  << std::endl;
	return;
}

Victim::Victim(Victim const & obj)
{
	*this = obj;
}

void Victim::operator=(const Victim & obj)
{
	this->_name = obj.get_name();
}

const std::string &Victim::get_name() const
{
	return _name;
}

std::ostream& operator<<(std::ostream& os, Victim const & obj)
{
	os << "I'm "
	   << obj.get_name()
	   << " and I like otters !"
	   << std::endl;
	return os;
}

void Victim::getPolymorphed() const
{
	std::cout << this->_name
			  << " has been turned into a cute little sheep !"
			  << std::endl;
}
