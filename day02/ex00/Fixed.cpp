//
// Created by Anastasiia Trepyton on 5/6/17.
//
#include <iostream>
#include "Fixed.hpp"
Fixed::Fixed()
{
	this->value = 0;
	std::cout << "Default constructor called\n";
	return;
}

Fixed::~Fixed()
{
	std::cout << "Destructor called\n";
	return;
}

Fixed::Fixed(Fixed const &src)
{
	std::cout << "Copy constructor called\n";
	this->value = src.getRawBits();
}

void Fixed::operator=(const Fixed &obj)
{
	std::cout << "Assignation operator called\n";
	this->setRawBits(obj.getRawBits());
}

int Fixed::getRawBits( void ) const
{
	std::cout << "getRawBits member function called\n";
	return this->value;

}
void Fixed::setRawBits( int const raw )
{
	this->value = raw;
}