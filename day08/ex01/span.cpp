#include "span.hpp"

Span::Span(unsigned int size) : _size(size) {
}

Span::Span() {
}

Span::~Span() {}

Span::Span(const Span &obj) {
    *this = obj;
}

Span &Span::operator=(const Span &obj) {
    _size = obj._size;
    _span = obj._span;
    return *this;
}

void Span::addNumber(unsigned int nb) {
    if (_span.size() < _size)
    {
        _span.push_back(nb);
    }
    else
    {
        //throw exceed size exception
    }
}

unsigned int Span::shortestSpan() {
    unsigned int shortest_span = 0;
    if(_span.empty())
    {
        //throw empty container exception
    }
    else if (_span.size() == 1)
    {
        //throw only one number in container exception
    }
    else
    {
        std::vector<unsigned int> sorted = _span;
        std::sort (sorted.begin(), sorted.end());

        std::vector<unsigned int>::iterator it;


        for(it = _span.begin(); it != _span.end() - 1; it++ )
        {
            if (shortest_span == 0)
            {
                shortest_span = *(it + 1) - *it;
            }
            else if (*(it + 1) - *it < shortest_span)
            {
                shortest_span = *(it + 1) - *it;
            }
        }
    }
    return shortest_span;
}

unsigned int Span::longestSpan() {
    if(_span.empty())
    {
        //throw empty container exception
    }
    else if (_span.size() == 1)
    {
        //throw only one number in container exception
    }
    else
    {
        unsigned int min = *std::min_element(_span.begin(), _span.end());
        unsigned int max = *std::max_element(_span.begin(), _span.end());
        return max-min;
    }
}
