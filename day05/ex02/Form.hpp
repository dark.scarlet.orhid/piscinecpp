#ifndef FORM_HPP
# define FORM_HPP


#include <string>
#include <ostream>
#include "Bureaucrat.hpp"
class Bureaucrat;
class Form {

public:
	Form();
	Form(const std::string &name, int grade_sign, int grade_execute, std::string target);
	Form(Form const & rhs);
	virtual ~Form();
	Form & operator=(Form const & rhs);

	const std::string &getName() const;
	bool isSign() const;
	int getGrade_execute() const;
	int getGrade_sign() const;
	const std::string &getTarget() const;

	void beSigned(Bureaucrat const & b);

	virtual void execute(Bureaucrat const & executor) const = 0;

	struct FormIsNorSignedException : public std::exception
    {
		virtual const char * what() const throw();
    };
private:
	const std::string  name;
	bool sign;
	const int grade_execute;
	const int grade_sign;
	std::string target;
};

std::ostream &operator<<(std::ostream &os, const Form &form);



#endif //NEWCPP_FORM_HPP
