class Fixed
{
private:
	int value;
	static const int bits = 8;
public:
	Fixed();
	Fixed(const Fixed &obj);
	~Fixed();
	void operator=(const Fixed &obj);
	int getRawBits( void ) const;
	void setRawBits( int const raw );
};