#include <iostream>
#include "Form.hpp"

Form::Form() : name("default"), sign(false),  grade_execute(1), grade_sign(150)  { return ; }

Form::Form(const std::string &name, int grade_sign, int grade_execute) :  name(name), grade_execute(grade_execute), grade_sign(grade_sign)
{
	this->sign = false;
	try
	{
		if (this->grade_sign > 150 || this->grade_execute > 150)
			throw (Bureaucrat::GradeTooLowException());
		else if (this->grade_sign < 1 || this->grade_execute < 1)
			throw (Bureaucrat::GradeTooHighException());
	}
	catch (std::exception & e)
	{
		std::cout << "Exception occurred: " << e.what() << std::endl;
	}
	return ;
}

Form::~Form() { return; }

Form::Form(Form const &rhs) : name(rhs.getName()), sign(rhs.isSign()), grade_execute(rhs.getGrade_execute()), grade_sign(rhs.getGrade_sign())
{ return ; }

Form & Form::operator=(Form const &rhs)
{
	this->sign = rhs.isSign();
	return *this;
}

//_______________________Getter_______________________//
const std::string &Form::getName() const
{
	return name;
}

bool Form::isSign() const
{
	return sign;
}

int Form::getGrade_execute() const
{
	return grade_execute;
}

int Form::getGrade_sign() const
{
	return grade_sign;
}


//_____________________Methods____________________________//

void Form::beSigned(Bureaucrat const &b)
{
	try
	{
		if (b.getGrade() <= this->grade_sign)
		{
			this->sign = true;
		}
		else
		{
			throw Bureaucrat::GradeTooLowException();
		}
		b.signForm(*this);
	}
	catch (std::exception & e)
	{
		std::cout << "Exception occurred: " << e.what() << std::endl;
	}
}


std::ostream &operator<<(std::ostream &os, const Form &form)
{
	os << "name: " << form.getName() << ", sign: " << form.isSign() << ", grade_execute: "
	   << form.getGrade_execute() << ", grade_sign: " << form.getGrade_sign();
	return os;
}
