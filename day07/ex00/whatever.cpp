#include <iostream>
#include <string>

template<class T>
void swap(T& v1, T& v2)
{
	T temp = v1;
	v1 = v2;
	v2 = temp;
}

template<class T>
const T& min(const T& v1, const T& v2)
{
	return v1 < v2 ? v1 : v2;
}


template<class T>
const T& max(const T& v1, const T& v2)
{
	return v1 > v2 ? v1 : v2;
}

int main(void) {
	int a = 2; 
	int b = 3;

	::swap(a, b); 
	std::cout << "a = " << a << ", b = " << b << std::endl;
	std::cout << "min( a, b ) = " << ::min(a, b) << std::endl;
	std::cout << "max( a, b ) = " << ::max(a, b) << std::endl;

	std::string c = "chaine1";
	std::string d = "chaine2";

	::swap(c, d);
	std::cout << "c = " << c << ", d = " << d << std::endl;
	std::cout << "min( c, d ) = " << ::min(c, d) << std::endl;
	std::cout << "max( c, d ) = " << ::max(c, d) << std::endl;
	return 0;
}
