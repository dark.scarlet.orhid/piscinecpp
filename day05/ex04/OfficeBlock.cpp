//
// Created by Anastasiia Trepyton on 11/4/17.
//

#include "OfficeBlock.hpp"



void OfficeBlock::doBureaucracy(std::string formName, std::string target) {

    if (this->signer && this->executor && this->intern)
    {
        Form *newForm = this->intern->makeForm(formName, target);
        if (newForm) {
            this->signer->signForm(*newForm);
            this->executor->executeForm(*newForm);
        }
    }
    else
    {
        if (!this->intern)
            throw OfficeBlock::InternNotAtWork();
        else if (!this->executor)
            throw OfficeBlock::ExecutingBureaucratNotAtWork();
        else if (!this->signer)
            throw OfficeBlock::SigningBureaucratNotAtWork();
    }
}

OfficeBlock::OfficeBlock() {
    this->intern = nullptr;
    this->signer = nullptr;
    this->executor = nullptr;
    return;
}

OfficeBlock::~OfficeBlock() {
    return;
}


void OfficeBlock::setIntern( Intern &intern) {
    this->intern = &intern;
}

void OfficeBlock::setExecutor(Bureaucrat &executor) {
    this->executor = &executor;
}

void OfficeBlock::setSigner(Bureaucrat &signer) {
    this->signer = &signer;
}

OfficeBlock::OfficeBlock(Intern *intern, Bureaucrat *executor, Bureaucrat *signer) : intern(intern), executor(executor),
                                                                                     signer(signer) {}

OfficeBlock &OfficeBlock::operator=(OfficeBlock const &rhs) {
    this->signer = rhs.signer;
    this->executor = rhs.executor;
    this->intern = rhs.intern;
    return *this;
}

OfficeBlock::OfficeBlock(OfficeBlock const &src) {
 *this = src;
}


//__Exceptions__
const char *OfficeBlock::InternNotAtWork::what() const throw()
{
    return "No Intern in the office";
}

const char *OfficeBlock::ExecutingBureaucratNotAtWork::what() const throw()
{
    return "There is nobody to execute the form";
}

const char *OfficeBlock::SigningBureaucratNotAtWork::what() const throw()
{
    return "There is nobody to sign the form";
}