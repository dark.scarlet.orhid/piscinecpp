//
// Created by Anastasiia Trepyton on 11/4/17.
//

#include <iostream>
#include "RobotomyRequestForm.hpp"
#include "Form.hpp"

int RobotomyRequestForm::switchResult = 0;

RobotomyRequestForm::RobotomyRequestForm() : Form()
{
    return;
}

RobotomyRequestForm::~RobotomyRequestForm() {
    return;
}

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const &obj) {
    *this = obj;
}

RobotomyRequestForm &RobotomyRequestForm::operator=(RobotomyRequestForm const &rhs) {
    if (&rhs != this)
        return *this;
    return *this;
}

RobotomyRequestForm::RobotomyRequestForm(std::string target) : Form("RobotomyRequestForm", 72, 45, target) {
    return ;
}

void RobotomyRequestForm::execute(Bureaucrat const &executor) const {

    try {
        if (this->isSign() && executor.getGrade() <= this->getGrade_execute()) {
            std::cout << "Drrrr-drrrr-drrrr" << std::endl;
            if (RobotomyRequestForm::switchResult) {
                std::cout << this->getTarget() << " has been robotomized successfully" << std::endl;
                RobotomyRequestForm::switchResult = 0;
            } else {
                std::cout << this->getTarget() << " robotomization failed" << std::endl;
                RobotomyRequestForm::switchResult = 1;
            }
        }
        else{
            if (this->isSign()) {
                throw Bureaucrat::GradeTooLowException();
            } else if (executor.getGrade() > this->getGrade_execute()) {
                throw Form::FormIsNorSignedException();
            }
        }
    }
    catch (std::exception & e)
    {
        std::cout << "Exception occurred: " << e.what() << std::endl;
    }
}

