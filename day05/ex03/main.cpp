
#include <iostream>
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"

int main()
{
	Intern intern;
    try {
        Form *rrf = intern.makeForm("robotomy request", "random target");
        std::cout << *rrf << std::endl;
        Form *scf = intern.makeForm("shrubbery creation", "random target");
        std::cout << *scf << std::endl;
        Form *ppf = intern.makeForm("presidential pardon", "random target");
        std::cout << *ppf << std::endl;
        Form *invalid_f = intern.makeForm("piscine ++", "random target");
        if (!invalid_f) {
            printf("not created\n");
        } else {
            printf("created\n");
        }
    }
    catch (std::exception & e)
    {
        std::cout << "Exception occurred: " << e.what() << std::endl;
    }
    return 0;
}