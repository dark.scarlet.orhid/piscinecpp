//
// Created by Anastasiia Trepyton on 5/10/17.
//

#ifndef NEWCPP_RADSCORPION_HPP
#define NEWCPP_RADSCORPION_HPP


#include "Enemy.hpp"

class RadScorpion : public Enemy
{
public:
	RadScorpion();

	virtual ~RadScorpion();

	RadScorpion(RadScorpion const & obj);
};

#endif //NEWCPP_RADSCORPION_HPP
