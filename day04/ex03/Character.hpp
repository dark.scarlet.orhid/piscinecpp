#include "ICharacter.hpp"

class Character : public ICharacter
{
public:
    Character();
    Character(const Character & obj);
    Character &operator=(const Character & obj);

    virtual ~Character();

    const std::string &getName() const override;

    void equip(AMateria *m) override;

    void unequip(int idx) override;

    void use(int idx, ICharacter &target) override;


};