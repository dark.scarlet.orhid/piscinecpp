#include <string>
#include <iostream>
class Fixed
{
private:
	int value;
	static const int bits = 8;
public:
	Fixed();
	Fixed(const Fixed &obj);
	Fixed(const int nb);
	Fixed(const float nb);
	~Fixed();
	void operator=(const Fixed &obj);
	int getRawBits( void ) const;
	void setRawBits( int const raw );

	float toFloat( void ) const;
	int toInt( void ) const;

};

std::ostream& operator<<(std::ostream& os, Fixed const & obj);