#include "FragTrap.hpp"
#include <string>
#include <iostream>
#include <ctime>

FragTrap::FragTrap(){return;}

FragTrap::FragTrap(const std::string name)
{
	srand(time(0));
	this->name = name;
	this->hitPoints = 100;
	this->maxHitPoints = 100;
	this->energyPoints = 100;
	this->maxEnergyPoints = 100;
	this->level = 1;
	this->meleeAttackDamage = 30;
	this->rangedAttackDamage = 20;
	this->armorDamageReduction = 5;
	std::cout << "FR4G-TP " << this->name << " constructed" << std::endl;
	return;
}

FragTrap::~FragTrap() {
	std::cout << "FR4G-TP " << this->name << " destructed"<< std::endl;
	return;
}

FragTrap::FragTrap(const FragTrap &rhs)
{
	*this = rhs;
	return;
}

void FragTrap::operator=(const FragTrap & rhs)
{
	this->name = rhs.getName();
	this->hitPoints = rhs.getHitPoints();
	this->maxHitPoints = rhs.getMaxHitPoints();
	this->energyPoints = rhs.getEnergyPoints();
	this->maxEnergyPoints = rhs.getMaxEnergyPoints();
	this->level = getLevel();
	this->meleeAttackDamage = getMeleeAttackDamage();
	this->rangedAttackDamage = getRangedAttackDamage();
	this->armorDamageReduction = getArmorDamageReduction();
}

void FragTrap::rangedAttack(std::string const & target)
{
	if (this->getHitPoints() >= this->getRangedAttackDamage())
	{
		std::cout << "FR4G-TP " << this->getName() << " attacks " << target
				  << " at range, causing " << this->getRangedAttackDamage()
				  << " points of damage !" << std::endl;
		this->takeDamage(this->getRangedAttackDamage());
	}
	else
		std::cout << "Not enough points to do ranged attack." << std::endl;
}

void FragTrap::meleeAttack(std::string const & target)
{
	if (this->getHitPoints() >= this->getMeleeAttackDamage())
	{
		std::cout << "FR4G-TP " << this->getName() << " attacks " << target
				  << " at melee, causing " << this->getMeleeAttackDamage()
				  << " points of damage !" << std::endl;
		this->takeDamage(this->getMeleeAttackDamage());
	}
	else
		std::cout << "Not enough points to do melee attack." << std::endl;
}

void FragTrap::takeDamage(unsigned int amount)
{
	if (this->getHitPoints() - amount + this->getArmorDamageReduction() > 0)
	{
		this->hitPoints = this->getHitPoints() - amount + this->getArmorDamageReduction();
		std::cout << "FR4G-TP "<< this->getName()  <<" lost "
				  << amount - this->getArmorDamageReduction()
				  << " points"<< std::endl;
	}
	else
	{
		std::cout << amount << " points can't be taken."<< std::endl;
	}
}

void FragTrap::beRepaired(unsigned int amount)
{
	if (this->getHitPoints() + amount <= this->getMaxHitPoints())
	{
		this->hitPoints= this->getHitPoints() + amount;
		std::cout << "FR4G-TP was repaired with " << amount
				  << " points"<< std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()
				  << " can't be repaired. Amount exceeds max number of points"
				  << std::endl;
	}
}

void FragTrap::conditionerAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at conditioner, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP " << this->getName()  <<" is out of energy." << std::endl;
	}
}

void FragTrap::segfaultAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at segfault, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()  <<" is out of energy." << std::endl;
	}
}

void FragTrap::busErrorAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at bus error, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()  <<" is out of energy." << std::endl;
	}
}

void FragTrap::vimAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at vim, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()  <<" is out of energy."
				  << std::endl;
	}
}

void FragTrap::emacsAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at emacs, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()  << " is out of energy."
				  << std::endl;
	}
}

void FragTrap::vaulthunter_dot_exe(std::string const & target)
{
	void(FragTrap::*point[5]) (std::string const &) = {&FragTrap::conditionerAttack,
	&FragTrap::segfaultAttack, &FragTrap::busErrorAttack, &FragTrap::vimAttack,
	&FragTrap::emacsAttack};
	(this->*point[rand() % 5])(target);
}


int FragTrap::getHitPoints() const { return hitPoints; }

int FragTrap::getMaxHitPoints() const { return maxHitPoints; }

int FragTrap::getEnergyPoints() const { return energyPoints; }

int FragTrap::getMaxEnergyPoints() const { return maxEnergyPoints; }

int FragTrap::getLevel() const { return level; }

int FragTrap::getMeleeAttackDamage() const { return meleeAttackDamage; }

int FragTrap::getRangedAttackDamage() const { return rangedAttackDamage; }

int FragTrap::getArmorDamageReduction() const { return armorDamageReduction; }

const std::string &FragTrap::getName() const { return name; }