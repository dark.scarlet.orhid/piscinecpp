#ifndef NEWCPP_FORM_HPP
#define NEWCPP_FORM_HPP


#include <string>
#include <ostream>
#include "Bureaucrat.hpp"
class Bureaucrat;
class Form {

public:
	Form();
	Form(const std::string &name, int grade_sign, int grade_execute);
	Form(Form const & rhs);
	virtual ~Form();

	Form & operator=(Form const & rhs);
	
	const std::string &getName() const;

	bool isSign() const;

	int getGrade_execute() const;

	int getGrade_sign() const;

	void beSigned(Bureaucrat const & b);

private:
	const std::string  name;
	bool sign;
	const int grade_execute;
	const int grade_sign;

};

std::ostream &operator<<(std::ostream &os, const Form &form);



#endif //NEWCPP_FORM_HPP
