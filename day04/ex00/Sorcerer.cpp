#include "Sorcerer.hpp"
#include <iostream>

Sorcerer::Sorcerer()
{
	this->_name = "Default name";
	this->_title = "Default type";
	return;
}

Sorcerer::Sorcerer(std::string name, std::string title)
{
	this->_name = name;
	this->_title = title;
	std::cout << this->_name
			  << ", "
			  << this->_title
			  << ", is born !"
			  << std::endl;
	return;
}

Sorcerer::Sorcerer(Sorcerer const & obj)
{
	*this = obj;
}

Sorcerer::~Sorcerer()
{
	std::cout << this->_name
			  << ", "
			  << this->_title
			  << ", is dead. Consequences will never be the same !"
			  << std::endl;
	return;
}

void Sorcerer::operator=(const Sorcerer & obj)
{
	this->_name = obj.get_name();
	this->_title = obj.get_title();
}

const std::string &Sorcerer::get_name() const
{
	return _name;
}

const std::string &Sorcerer::get_title() const
{
	return _title;
}

std::ostream& operator<<(std::ostream& os, Sorcerer const & obj)
{
	os	<< "I am "
		<< obj.get_name()
		<< ", "
		<< obj.get_title()
		<< ", and I like ponies !"
		<< std::endl;
	return os;
}

void Sorcerer::polymorph(Victim const & obj) const
{
	obj.getPolymorphed();
}