#ifndef CONTACT_HPP
# define CONTACT_HPP
#include <iomanip>
#include <iostream>
#include <string>

class Contact
{
private:
	std::string first_name;
	std::string last_name;
	std::string nickname;
	std::string login;
	std::string address;
	std::string email;
	std::string phone_number;
	std::string birthday;
	std::string favorite_meal;
	std::string underwear_color;
	std::string darkest_secret;

public:
	Contact(void);
	~Contact(void);

	const std::string &getFirst_name() const;
	const std::string &getLast_name() const;
	const std::string &getNickname() const;
	const std::string &getLogin() const;
	const std::string &getAddress() const;
	const std::string &getEmail() const;
	const std::string &getPhone_number() const;
	const std::string &getBirthday() const;
	const std::string &getFavorite_meal() const;
	const std::string &getUnderwear_color() const;
	const std::string &getDarkest_secret() const;

	void setFirstName(std::string name);
	void setLastName(std::string name);
	void setNickname(std::string name);
	void setLogin(std::string name);
	void setAddress(std::string name);
	void setEmail(std::string name);
	void setPhoneNumber(std::string name);
	void setBirthday(std::string name);
	void setFavouriteMeal(std::string name);
	void setUnderwearColor(std::string name);
	void setDarkestSecret(std::string name);

	void toString();
	static Contact receiveContactInfo();
};

#endif