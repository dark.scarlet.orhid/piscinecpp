#include "Logger.hpp"

int main()
{
	Logger start("program.log");

	start.log("logToConsole", "This log should be put to console");
	start.log("logToFile", "And this log should be put to file");
	start.log("logToFile", "One more log to the file");
	return 0;
}