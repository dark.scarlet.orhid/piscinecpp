#include "NinjaTrap.hpp"
#include <iostream>
#include <string>

NinjaTrap::NinjaTrap() {return;}

NinjaTrap::NinjaTrap(const std::string name)
{
	this->name = name;
	this->type = "NinjaTrap";
	this->hitPoints = 60;
	this->maxHitPoints = 60;
	this->energyPoints = 120;
	this->maxEnergyPoints = 120;
	this->level = 1;
	this->meleeAttackDamage = 60;
	this->rangedAttackDamage = 5;
	this->armorDamageReduction = 0;
	std::cout << "NinjaTrap " << this->name << " constructed" << std::endl;
	return;
}

NinjaTrap::NinjaTrap(const NinjaTrap &rhs)
{
	*this = rhs;
	return;
}

NinjaTrap::~NinjaTrap()
{
	std::cout << "NinjaTrap " << this->name << " destructed" << std::endl;
}

void NinjaTrap::ninjaShoebox(FragTrap & obj)
{
	obj.vaulthunter_dot_exe("CLion");
	std::cout << "Have no idea what this fucking function is doing !"
			  << std::endl;
}

void NinjaTrap::ninjaShoebox(ScavTrap & obj)
{
	obj.challengeNewcomer();
	std::cout << "What a nice challenge, isn't it ?!"
			  << std::endl;
}

void NinjaTrap::ninjaShoebox(NinjaTrap & obj)
{
	obj.meleeAttack("table tennis");
	std::cout << "I killed table tennis!!!"
			  << std::endl;
}

void NinjaTrap::ninjaShoebox(ClapTrap & obj)
{
	obj.rangedAttack("iMac");
	std::cout << "I broke this fucking iMac!!!"
			  << std::endl;
}