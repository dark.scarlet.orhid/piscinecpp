//
// Created by Anastasiia Trepyton on 11/4/17.
//

#include <iostream>
#include "PresidentialPardonForm.hpp"
#include "Form.hpp"

PresidentialPardonForm::PresidentialPardonForm() : Form()
{
    return;
}

PresidentialPardonForm::~PresidentialPardonForm() {
    return;
}

PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const &obj) {
    *this = obj;
}

PresidentialPardonForm &PresidentialPardonForm::operator=(PresidentialPardonForm const &rhs) {
    if (&rhs != this)
        return *this;
    return *this;
}

PresidentialPardonForm::PresidentialPardonForm(std::string target) : Form("PresidentialPardonForm", 25, 5, target){
    return ;
}

void PresidentialPardonForm::execute(Bureaucrat const &executor) const {

        if (this->isSign() && executor.getGrade() <= this->getGrade_execute()) {
            std::cout << this->getTarget() << " has been pardoned by Zafod Beeblebrox" << std::endl;
        } else{
             if (this->isSign()) {
                throw Bureaucrat::GradeTooLowException();
             } else if (executor.getGrade() > this->getGrade_execute()) {
                throw Form::FormIsNorSignedException();
            }
        }
}

