//
// Created by Anastasiia Trepyton on 11/5/17.
//

#ifndef DAY07_ARRAY_HPP
#define DAY07_ARRAY_HPP

#include <cstdlib>
#include <iostream>

template <class T> class Array {
public:
    Array<T>() {
        this->arr_size = 0;
        this->array = NULL;
    }

    Array<T>(unsigned int n)
    {
        this->arr_size = n;
        this->array = new T[n]();
    }
    
    Array<T>(const Array & arr)
    {
        *this = arr;
    }

    Array<T> & operator=(const Array<T> & arr)
    {
        arr_size = arr.size();
        delete(array);
        array = new T[arr_size];
        for (int i; i < arr_size; i++)
        {
            array[i] = arr.array[i];
        }
        return *this;
    }

    virtual ~Array<T>() {

    }

    unsigned int size()
    {
        return arr_size;
    }

    T & operator[](unsigned int index)
    {
        if (index < arr_size)
        {
            return this->array[index];
        }
        else
        {
            throw std::exception();
        }
    }
private:
    T *array;
    unsigned int arr_size;
};


#endif //DAY07_ARRAY_HPP
