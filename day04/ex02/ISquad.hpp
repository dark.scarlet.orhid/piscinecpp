//
// Created by Anastasiia Trepyton on 5/11/17.
//
#ifndef NEWCPP_ISQUAD_HPP
#define NEWCPP_ISQUAD_HPP

#include "ISpaceMarine.hpp"

class ISquad
{
public:
	virtual ~ISquad() {}
	virtual int getCount() const = 0;
	virtual ISpaceMarine* getUnit(int) const = 0;
	virtual int push(ISpaceMarine*) = 0;
};
#endif //NEWCPP_ISQUAD_HPP
