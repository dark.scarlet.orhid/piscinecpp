#include <string>

class Logger
{
private:
	std::string filename;
	void logToConsole(std::string msg);
	void logToFile(std::string msg);
	std::string makeLogEntry(std::string message);
public:
	Logger(std::string file);
	void log(std::string const & dest, std::string const & message);
};