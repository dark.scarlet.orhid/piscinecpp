#include <iostream>
#include <fstream>

std::string strtoupper(std::string str)
{
	for (int i = 0; i < str.length(); i++)
		str[i] = (char)std::toupper(str[i]);
	return str;
}

int main(int argc, char **argv)
{
	if (argc == 4)
	{
		std::string line;
		std::string s1 = argv[2];
		std::string s2 = argv[3];
		if (s1.empty() || s2.empty())
		{
			std::cout << "ERROR: One or both of the strings are empty." << std::endl;
			return (0);
		}
		std::string filename = argv[1];
		std::fstream toreplace;
		toreplace.open(filename, std::fstream::in);
		if (!toreplace.is_open())
		{
			std::cout << "ERROR: Can't open the file." << std::endl;
			return (0);
		}
		std::fstream newfile;
		filename = strtoupper(filename);
		filename += ".replace";
		newfile.open(filename, std::fstream::out);
		if (!newfile.is_open())
		{
			std::cout << "ERROR: Can't create the file." << std::endl;
			return (0);
		}
		while (getline(toreplace, line))
		{
			std::size_t found = line.find(s1, 0);
			while (found != std::string::npos)
			{
				line.replace(found, s1.length(), s2);
				found = line.find(s1, found + 1);
			}
			newfile << line << std::endl;
		}
		toreplace.close();
		newfile.close();
	}
	else
		std::cout << "Usage: " << argv[0]
				  << " <filename> <str to replace> <str to replace by>"
				  << std::endl;
	return (0);
}
