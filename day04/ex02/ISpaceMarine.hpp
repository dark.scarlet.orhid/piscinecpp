//
// Created by Anastasiia Trepyton on 5/11/17.
//

#ifndef NEWCPP_ISPACEMARINE_HPP
#define NEWCPP_ISPACEMARINE_HPP

#include <iostream>
class ISpaceMarine
{
public:
	virtual ~ISpaceMarine()  {} ;
	virtual ISpaceMarine* clone() const = 0;
	virtual void battleCry() const = 0;
	virtual void rangedAttack() const = 0;
	virtual void meleeAttack() const = 0;
};
#endif //NEWCPP_ISPACEMARINE_HPP
