#include "EvalExpr.hpp"
#include "Fixed.hpp"
EvalExpr::EvalExpr()
{
	return;
}

EvalExpr::EvalExpr(std::string expression)
{
    this->_expression = expression;
	return;
}

EvalExpr::~EvalExpr()
{
	return;
}

EvalExpr::EvalExpr(EvalExpr const &src)
{
	*this = src;
	return;
}

void EvalExpr::operator=(const EvalExpr &obj)
{
    this->_expression = obj->_expression;
}

