
#include "ZombieEvent.hpp"

Zombie *randomChump()
{
	srand (time(NULL));
	std::string name[10] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
	ZombieEvent *random = new ZombieEvent();
	random->setZombieType("Cordycep");
	Zombie *z = random->newZombie(name[rand() % 10]);
	z->announce();
	return z;
}

int main()
{
	ZombieEvent event1;
	event1.setZombieType("Voodoo");
	Zombie *z1 = event1.newZombie("asvirido");
	Zombie *z2 = event1.newZombie("odirivsa");
	Zombie *z3 = event1.newZombie("arywpofs");
	ZombieEvent event2;
	event2.setZombieType("Crawler");
	Zombie *z4 = event2.newZombie("123");
	Zombie *z5 = event2.newZombie("456");
	Zombie *z6 = event2.newZombie("789");
	z4->announce();
	z5->announce();
	z6->announce();
	Zombie *randz = randomChump();
}