#include "Brain.hpp"
class Human
{
	const Brain b;
	public:
		Human();
		~Human();
		std::string identify();
		const Brain& getBrain();
};