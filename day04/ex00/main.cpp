#include "Sorcerer.hpp"
#include "Victim.hpp"
#include "Peon.hpp"
#include <iostream>

int main()
{
	Sorcerer robert("Robert", "the Magnificent");
	Victim jim("Jimmy");
	Peon joe("Joe");
	std::cout << robert << jim << joe;
	robert.polymorph(jim);
	robert.polymorph(joe);

	std::cout << robert.get_name() << " " << robert.get_title() << std::endl;
	Peon a("Rainbow Dash");
	Victim d("Red");
	Victim *c = new Peon("ASasdasdasdasd");
	std::cout << a;
	std::cout << *c;
	std::cout << d;
	robert.polymorph(*c);
	robert.polymorph(a);
	robert.polymorph(d);
	return 0;
}