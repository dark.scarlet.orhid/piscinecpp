//
// Created by Anastasiia Trepyton on 5/10/17.
//

#include "Enemy.hpp"

Enemy::Enemy()
{ return ;}

Enemy::Enemy (int hp, std::string const & type)
{
	this->_hit_points = hp;
	this->_type = type;
	return ;
}

Enemy::~Enemy()
{ return ;}

int Enemy::getHP() const
{
	return _hit_points;
}

const std::string &Enemy::getType() const
{
	return _type;
}

void Enemy::takeDamage(int n)
{
	if (this->getHP() >= 0)
	{
		this->_hit_points -= n;
	}
}

void Enemy::operator=(Enemy const & obj)
{
	this->_hit_points = obj.getHP();
	this->_type = obj.getType();
}