#include "easyfind.hpp"
#include <iostream>
#include <vector>
#include <deque>
#include <list>
int main()
{
    std::vector<int> myvector(5);
    for (unsigned int i = 0; i < myvector.size(); i++)
    {
        myvector[i] = i;
    }
    std::cout << easyfind(myvector, 3) << std::endl;
    std::cout << easyfind(myvector, 4) << std::endl;
    std::cout << easyfind(myvector, 8) << std::endl;
    std::cout << easyfind(myvector, 10) << std::endl;
    

    std::deque<int> mydeque;
    for (int i=1; i<=5; i++) 
    {
        mydeque.push_back(i);
    }
    std::cout << easyfind(mydeque, 3) << std::endl;
    std::cout << easyfind(mydeque, 5) << std::endl;
    std::cout << easyfind(mydeque, 8) << std::endl;
    std::cout << easyfind(mydeque, 10) << std::endl;

    std::list<int> mylist (2,100);
    mylist.push_front (200);
    mylist.push_back (300);
    std::cout << easyfind(mylist, 100) << std::endl;
    std::cout << easyfind(mylist, 300) << std::endl;
    std::cout << easyfind(mylist, 8) << std::endl;
    std::cout << easyfind(mylist, 10) << std::endl;
    return 0;

}