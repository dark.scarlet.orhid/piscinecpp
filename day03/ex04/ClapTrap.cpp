#include "ClapTrap.hpp"
#include <iostream>
#include <string>
#include <ctime>
#include <stdlib.h>
#include <time.h>
ClapTrap::ClapTrap()
{
	srand(time(0));
	this->name = "default";
	this->hitPoints = 0;
	this->maxHitPoints = 0;
	this->energyPoints = 0;
	this->maxEnergyPoints = 0;
	this->level = 0;
	this->meleeAttackDamage = 0;
	this->rangedAttackDamage = 0;
	this->armorDamageReduction = 0;
	std::cout << "ClapTrap " << " constructed" << std::endl;
	return;
}

ClapTrap::ClapTrap(std::string name)
{
	srand(time(0));
	this->name = name;
	this->hitPoints = 0;
	this->maxHitPoints = 0;
	this->energyPoints = 0;
	this->maxEnergyPoints = 0;
	this->level = 0;
	this->meleeAttackDamage = 0;
	this->rangedAttackDamage = 0;
	this->armorDamageReduction = 0;
	std::cout << "ClapTrap " << this->name <<" constructed" << std::endl;
	return;
}

ClapTrap::~ClapTrap() {
	std::cout << "ClapTrap destructed"<< std::endl;
	return;
}

ClapTrap::ClapTrap(const ClapTrap &rhs)
{
	*this = rhs;
	return;
}

void ClapTrap::operator=(const ClapTrap & rhs)
{
	this->name = rhs.getName();
	this->hitPoints = rhs.getHitPoints();
	this->maxHitPoints = rhs.getMaxHitPoints();
	this->energyPoints = rhs.getEnergyPoints();
	this->maxEnergyPoints = rhs.getMaxEnergyPoints();
	this->level = getLevel();
	this->meleeAttackDamage = getMeleeAttackDamage();
	this->rangedAttackDamage = getRangedAttackDamage();
	this->armorDamageReduction = getArmorDamageReduction();
}

int ClapTrap::getHitPoints() const { return hitPoints; }

int ClapTrap::getMaxHitPoints() const { return maxHitPoints; }

int ClapTrap::getEnergyPoints() const { return energyPoints; }

int ClapTrap::getMaxEnergyPoints() const { return maxEnergyPoints; }

int ClapTrap::getLevel() const { return level; }

int ClapTrap::getMeleeAttackDamage() const { return meleeAttackDamage; }

int ClapTrap::getRangedAttackDamage() const { return rangedAttackDamage; }

int ClapTrap::getArmorDamageReduction() const { return armorDamageReduction; }

const std::string &ClapTrap::getName() const { return name; }

void ClapTrap::rangedAttack(std::string const & target)
{
	if (this->getHitPoints() >= this->getRangedAttackDamage())
	{
		std::cout << "ClapTrap "
				  <<  this->getName()
				  << " attacks "
				  << target
				  << " at range, causing "
				  << this->getRangedAttackDamage()
				  << " points of damage !"
				  << std::endl;
		this->takeDamage(this->getRangedAttackDamage());
	}
	else
		std::cout << "Not enough points to do ranged attack."
				  << std::endl;
}

void ClapTrap::meleeAttack(std::string const & target)
{
	if (this->getHitPoints() >= this->getMeleeAttackDamage())
	{
		std::cout << "ClapTrap "
				  <<  this->getName()
				  << " attacks "
				  << target
				  << " at melee, causing "
				  << this->getMeleeAttackDamage()
				  << " points of damage !"
				  << std::endl;
		this->takeDamage(this->getMeleeAttackDamage());
	}
	else
		std::cout << "Not enough points to do melee attack."
				  << std::endl;
}

void ClapTrap::takeDamage(unsigned int amount)
{
	if (this->getHitPoints() - amount + this->getArmorDamageReduction() > 0)
	{
		this->hitPoints = this->getHitPoints() - amount + this->getArmorDamageReduction();
		std::cout << "ClapTrap "
				  << this->getName()
				  << " lost "
				  << amount - this->getArmorDamageReduction()
				  << " points"
				  << std::endl;
	}
	else
	{
		std::cout << amount
				  << " points can't be taken."
				  << std::endl;
	}
}

void ClapTrap::beRepaired(unsigned int amount)
{
	if (this->getHitPoints() + amount <= this->getMaxHitPoints())
	{
		this->hitPoints= this->getHitPoints() + amount;
		std::cout << "ClapTrap was repaired with "
				  << amount
				  << " points"<< std::endl;
	}
	else
	{
		std::cout << "ClapTrap "
				  << this->getName()
				  << " can't be repaired. Amount exceeds max number of points"
				  << std::endl;
	}
}
