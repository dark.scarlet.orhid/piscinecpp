#include "Squad.hpp"
Squad::Squad()
{
	this->arr = 0;
	this->count = 0;
	return;
}

Squad::~Squad()
{
	deleteArray(this->arr);
	return;
}

Squad::Squad(Squad const &obj)
{
	deleteArray(this->arr);
	*this = obj;
	return ;
}

void Squad::operator=(Squad const &obj)
{
	deleteArray(this->arr);
	this->arr = obj.arr;
}

int Squad::getCount() const
{
	return this->count;
}

ISpaceMarine *Squad::getUnit(int i) const
{
	return this->arr[i];
}

int Squad::push(ISpaceMarine *marine)
{
	ISpaceMarine **tmp = new ISpaceMarine*[this->count + 1];
	int i = 0;
	while (i < this->count)
	{
		tmp[i] = this->arr[i]->clone();
		delete this->arr[i];
		i++;
	}
	tmp[this->count] = marine->clone();
	this->count++;
	this->arr = tmp;
	return this->count;
}

void Squad::deleteArray(ISpaceMarine **arr)
{
	int i = 0;
	while (i < this->count)
	{
		delete arr[i];
		i++;
	}
	delete[] arr;
}
