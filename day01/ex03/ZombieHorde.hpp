#include <string>
#include <iostream>
#include "Zombie.hpp"
class ZombieHorde
{

	Zombie *horde;

	public:
		ZombieHorde(int n);
		~ZombieHorde();
		void announce();
};