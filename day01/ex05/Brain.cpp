//
// Created by Anastasiia Trepyton on 5/5/17.
//
#include <string>
#include <sstream>
#include "Brain.hpp"

Brain::Brain()
{
	return;
}

Brain::~Brain()
{
	return;
}

const std::string Brain::identify() const
{
	std::stringstream ss;
	ss << this;
	return ss.str();
}
