//
// Created by Anastasiia Trepyton on 11/5/17.
//
#include <iostream>
#include "Serialization.hpp"
int main()
{
    Serialization serialiser;

    try {
        void *res = serialiser.serialize();
        if (res)
        {
            std::cout <<"Object was serialized" << std::endl;
        }
        Serialization::Data *data = serialiser.deserialize(res);
        if (data) {
            std::cout << "Object was derialized" << std::endl;
            std::cout << "s1 " << data->s1 << std::endl
                      << "nb " << data->n << std::endl
                      << "s2 " << data->s2 << std::endl;
        }
    }
    catch (std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }

}