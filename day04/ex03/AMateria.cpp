#include "AMateria.hpp"

AMateria::AMateria() {
    return;
}

AMateria::~AMateria() {
    return;
}

AMateria::AMateria(std::string const &type) {
    this->materialType_ = type;
    return;
}

AMateria::AMateria(const AMateria &obj) {
    *this = obj;
    return ;
}

AMateria &AMateria::operator=(const AMateria &obj) {
    this->materialType_ = obj.materialType_;
    this->xp_ = obj.xp_;
    return *this;
}

std::string const &AMateria::getType() const {
    return this->materialType_;
}

unsigned int AMateria::getXP() const {
    return this->xp_;
}

void AMateria::use(ICharacter &target) {
    this->xp_ += 10;
}
