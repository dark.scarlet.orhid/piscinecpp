
#include "Cure.hpp"

Cure::Cure() : AMateria("cure") {
    return ;
}

Cure::Cure(const Cure &obj) {
    *this = obj;
}

Cure &Cure::operator=(const Cure &obj) {
    return *this;
}

Cure::~Cure() {
    return;
}

AMateria *Cure::clone() const {
    return nullptr;
}

void Cure::use(ICharacter &target) {
    AMateria::use(target);
    std::cout << "* heals "<<  target.getName() << "’s wounds *" << std::endl;

}
