#include "PhoneBook.hpp"
PhoneBook::PhoneBook()
{
	PhoneBook::numberOfContacts = 0;
	return;
}

PhoneBook::~PhoneBook()
{
	return;
}

void PhoneBook::openPhonebook()
{
	std::cout << "List of available commands: ADD, SEARCH, EXIT.\n"
		"Enter your command:" << std::endl;
	std::string action;
	if (std::getline(std::cin, action) == 0) 
	{
		exit(1);
	}
	if (action == "ADD")
	{
		PhoneBook::addContact();

	}
	else if (action == "SEARCH")
	{
		PhoneBook::displayContacts();
	}
	else if (action == "EXIT")
	{
		PhoneBook::exitPhonebook();
	}
}



void PhoneBook::addContact()
{
	if (PhoneBook::numberOfContacts >= 8)
	{
		std::cout << "Phone Book is full" << std::endl;
	}
	else
	{
		Contact newContact = Contact::receiveContactInfo();
		PhoneBook::elements[PhoneBook::numberOfContacts] = newContact;
		PhoneBook::numberOfContacts++;
	}
}

std::string PhoneBook::formatContactOutput(std::string toFormat)
{
	if (toFormat.length() > 10)
	{
		toFormat.resize(10);
		toFormat = toFormat.substr(0, toFormat.length() - 1);
		toFormat.push_back('.');
	}
	return toFormat;
}

void PhoneBook::displayContacts()
{

	if (PhoneBook::numberOfContacts == 0)
	{
		std::cout << "The PhoneBook is totally empty!" << std::endl;
	}
	else {
		std::cout << "     index|first name| last name|  nickname" << std::endl;
		for (int j = 0; j < PhoneBook::numberOfContacts; j++)
		{
			std::string name = PhoneBook::formatContactOutput(elements[j].getFirst_name());
			std::string lastname = PhoneBook::formatContactOutput(elements[j].getLast_name());
			std::string nickname = PhoneBook::formatContactOutput(elements[j].getNickname());
			std::cout << std::setw(10);
			std::cout << j << "|";
			std::cout << std::setw(10);
			std::cout << name << "|";
			std::cout << std::setw(10);
			std::cout << lastname << "|";
			std::cout << std::setw(10);
			std::cout << nickname << std::endl;
		}
		PhoneBook::searchContactByIndex();
	}
}

void PhoneBook::searchContactByIndex()
{

	std::string index;
	std::cout << "Enter index of element" << std::endl;
	std::getline(std::cin, index);
	if (index.length() == 1 && isdigit(index[0]))
	{
		if (index[0] - 48 <= PhoneBook::numberOfContacts)
		{
			elements[index[0] - 48].toString();
		}
		else
		{
			std::cout << "There is no such element" << std::endl;
		}
	}
	else {
		std::cout << "The value of index invalid" << std::endl;
	}

}

void PhoneBook::exitPhonebook()
{
	exit(0);
}