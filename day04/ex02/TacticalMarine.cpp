#include "TacticalMarine.hpp"

ISpaceMarine *TacticalMarine::clone() const
{

	ISpaceMarine *a = new TacticalMarine(*this);
	return a;
}

void TacticalMarine::battleCry() const
{
	std::cout << "For the holy PLOT !" << std::endl;
}

void TacticalMarine::rangedAttack() const
{
	std::cout << "* attacks with bolter *" << std::endl;
}

void TacticalMarine::meleeAttack() const
{
	std::cout << "* attacks with chainsword *" << std::endl;
}

TacticalMarine::TacticalMarine()
{
	std::cout << "Tactical Marine ready for battle" << std::endl;
	return ;
}

TacticalMarine::~TacticalMarine()
{
	std::cout << "Aaargh ..." << std::endl;
	return;
}

TacticalMarine::TacticalMarine(TacticalMarine const &obj)
{
	*this = obj;
}

void TacticalMarine::operator=(TacticalMarine const &obj)
{
	return;
}
