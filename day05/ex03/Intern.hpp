//
// Created by Anastasiia Trepyton on 11/4/17.
//

#ifndef DAY05_INTERN_HPP
#define DAY05_INTERN_HPP

#include <string>
#include "Form.hpp"
class Intern
{
public:
    Intern();
    Intern(Intern const &obj);
    Intern &operator=(Intern const &obj);
    virtual ~Intern();
    Form *makeForm(std::string name, std::string target);

    struct NotValidFormException : public std::exception
    {
        virtual const char * what() const throw();
    };
};
#endif //DAY05_INTERN_HPP
