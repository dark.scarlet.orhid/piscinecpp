#include <iostream>
#include "Bureaucrat.hpp"
#include "../ex01/Form.hpp"

Bureaucrat::Bureaucrat() { return ; }

Bureaucrat::~Bureaucrat() {	return; }

Bureaucrat::Bureaucrat(std::string name, int grade)
{
	this->name = name;
	try
	{
		if (grade > 150)
			throw Bureaucrat::GradeTooLowException();
		else if (grade < 1)
			throw Bureaucrat::GradeTooHighException();
		else
			this->grade = grade;
	}
	catch (std::exception & e)
	{
		std::cout << "Exception occurred: " << e.what() << std::endl;
	}
	return ;
}

Bureaucrat::Bureaucrat(Bureaucrat const &obj)
{
	*this = obj;
	return ;
}

Bureaucrat & Bureaucrat::operator=(Bureaucrat const &obj)
{
	this->name = obj.getName();
	this->grade = obj.getGrade();
	return *this;
}

const std::string &Bureaucrat::getName() const
{
	return name;
}

const int &Bureaucrat::getGrade() const
{
	return grade;
}

void Bureaucrat::incrementGrade()
{
	try
	{
		if (grade - 1 < 1)
			throw Bureaucrat::GradeTooHighException();
		else
			this->grade--;
	}
	catch (std::exception & e)
	{
		std::cout << "Exception occurred: " << e.what() << std::endl;
	}
}

void Bureaucrat::decrementGrade()
{
	try
	{
		if (grade + 1 > 150)
			throw Bureaucrat::GradeTooLowException();
		else
			this->grade++;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception occurred: " << e.what() << std::endl;
	}
}


const char *Bureaucrat::GradeTooLowException::what() const throw()
{
	return "Too low grade";
}

const char *Bureaucrat::GradeTooHighException::what() const throw()
{
	return "Too high grade";
}


std::ostream &operator<<(std::ostream &os, const Bureaucrat &bureaucrat)
{
	os << bureaucrat.getName() << ", bureaucrat grade " << bureaucrat.getGrade();
	return os;
}
