#ifndef AMATERIA_HPP
#define AMATERIA_HPP

#include <string>
#include "ICharacter.hpp"
class AMateria
{
    private:
          std::string materialType_;
          unsigned int xp_;
    public:
          AMateria(std::string const & type);
          AMateria(const AMateria &obj);
          AMateria & operator=(const AMateria &obj);
          AMateria();
          ~AMateria();

        std::string const & getType() const; //Returns the materia type
        unsigned int getXP() const; //Returns the Materia s XP
        virtual AMateria* clone() const = 0;
        virtual void use(ICharacter& target);

};

#endif