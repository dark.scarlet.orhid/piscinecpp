#ifndef NEWCPP_SQUAD_HPP
#define NEWCPP_SQUAD_HPP

#include "ISquad.hpp"

class Squad : public ISquad
{
public:
	Squad();
	Squad(Squad const & obj);
	virtual ~Squad();
	void operator=(Squad const & obj);
	int getCount() const;
	ISpaceMarine *getUnit(int i) const;
	int push(ISpaceMarine *marine);

private:
	ISpaceMarine **arr;
	int count;
	void deleteArray(ISpaceMarine **arr);
};
#endif