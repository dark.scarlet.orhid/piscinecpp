//
// Created by Anastasiia Trepyton on 11/4/17.
//

#ifndef DAY05_PRESIDENTIALPARDONFORM_HPP
#define DAY05_PRESIDENTIALPARDONFORM_HPP
#include "Form.hpp"

class PresidentialPardonForm : public Form
{
public:
    PresidentialPardonForm();
    PresidentialPardonForm(std::string target);
    PresidentialPardonForm( PresidentialPardonForm const & obj);
    PresidentialPardonForm & operator=(PresidentialPardonForm const & rhs);
    virtual ~PresidentialPardonForm();

    void execute(Bureaucrat const &executor) const;

};
#endif //DAY05_PRESIDENTIALPARDONFORM_HPP
