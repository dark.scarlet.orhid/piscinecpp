#include "Human.hpp"
#include <iostream>
void Human::meleeAttack(std::string const & target)
{
	std::cout << target << " is attacked by melee attack" << std::endl;
}

void Human::rangedAttack(std::string const & target)
{
	std::cout << target << " is attacked by ranged attack" << std::endl;
}

void Human::intimidatingShout(std::string const & target)
{
	std::cout << target << " is attacked by intimidating shout" << std::endl;
}

typedef void(Human::*t_p) (std::string const &);

void Human::action(std::string const & action_name, std::string const & target)
{
	std::string actions[3] = {"meleeAttack", "rangedAttack", "intimidatingShout"};
	t_p ptof[3] = {&Human::meleeAttack, &Human::rangedAttack, &Human::intimidatingShout};
	int i;
	for( i = 0; i < 3; i++)
	{
		if (action_name.compare(actions[i]) == 0)
			break;
	}
	if (i < 3)
		(this->*ptof[i])(target);
}