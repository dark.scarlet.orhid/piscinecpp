#include "PlasmaRifle.hpp"
#include <iostream>

PlasmaRifle::PlasmaRifle() : AWeapon("Plasma Rifle", 5, 21)
{ return ;}

PlasmaRifle::PlasmaRifle(const PlasmaRifle &obj) : AWeapon(obj)
{ return ;}

PlasmaRifle::~PlasmaRifle()
{ return ; }

void PlasmaRifle::attack() const
{
	std::cout << "* piouuu piouuu piouuu *" << std::endl;
}