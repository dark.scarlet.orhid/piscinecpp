template <typename T>
bool easyfind(T container, int toFind)
{
    for (typename T::iterator it = container.begin() ; it != container.end() ; ++it)
    {
        if (*it == toFind)
        {
            return true;
        }
    }
    return false;
}