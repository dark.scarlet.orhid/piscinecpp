#include "Weapon.hpp"
#include <string>

class HumanB
{

public:
	Weapon *death;
	std::string name;
	HumanB(std::string n);
	~HumanB();
	void attack(void);
	void setWeapon(Weapon& w);
};
