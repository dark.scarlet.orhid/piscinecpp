#include <string>
class Brain
{
	int		age;
	float	iq;
	public:
		Brain();
		~Brain();
		const std::string identify() const;
};