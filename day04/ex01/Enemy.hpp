//
// Created by Anastasiia Trepyton on 5/10/17.
//

#ifndef NEWCPP_ENEMY_HPP
#define NEWCPP_ENEMY_HPP

#include <string>
class Enemy {
	private:
		int _hit_points;
		std::string _type;
	public:
		Enemy(int hp, std::string const & type);
		Enemy();
		virtual ~Enemy();
		void operator=(Enemy const & obj);
		std::string const & getType() const;
		int getHP() const;
		virtual void takeDamage(int n);
};


#endif //NEWCPP_ENEMY_HPP
