#include "Convertions.hpp"

Convertions::Convertions() {return;}

Convertions::Convertions(std::string str)
{
	this->toConvert = std::atof(str.c_str());
	this->pres = static_cast<int>(getPoint(str));
}

Convertions::Convertions(Convertions const & obj)
{
	*this = obj;
	return;
}

Convertions::~Convertions(){ return; }

void Convertions::operator=(Convertions const & obj)
{
	this->toConvert = obj.toConvert;
}

const char *Convertions::Impossible::what() const throw()
{
	return "impossible";
}

const char *Convertions::NonDisplayable::what() const throw()
{
	return "Non displayable";
}

void Convertions::convToChar()
{
	char res;
	try
	{
		std::cout << "char: ";
		if (std::isprint(this->toConvert) && this->toConvert > std::numeric_limits<char>::min() &&
			this->toConvert < std::numeric_limits<char>::max())
			res = static_cast<char>(this->toConvert);
		else if (std::isnan(this->toConvert) ||
				this->toConvert < std::numeric_limits<char>::min() ||
				this->toConvert > std::numeric_limits<char>::max())
			throw Convertions::Impossible();
		else
			throw Convertions::NonDisplayable();
		std::cout << "'" << res << "'" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}
}

void Convertions::convToInt()
{
	int res;
	try
	{
		std::cout << "int: ";
		if (this->toConvert < std::numeric_limits<int>::min() ||
				this->toConvert > std::numeric_limits<int>::max())
			throw Convertions::Impossible();
		else if (std::isnan(this->toConvert))
			throw Convertions::Impossible();
		else
			res = static_cast<int>(this->toConvert);
		std::cout << res << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}
}

void Convertions::convToDouble ()
{
	std::cout << "double: ";
	std::cout << std::setprecision(this->pres)<< std::fixed;
	std::cout << this->toConvert << std::endl;
}

void Convertions::convToFloat()
{
	float res;
	std::cout << "float: ";
	res = static_cast<float>(this->toConvert);
	std::cout << std::setprecision(this->pres) << std::fixed;
	std::cout << res << "f"<< std::endl;
}

size_t Convertions::getPoint(std::string str)
{
	std::size_t found = str.find(".");
	if (found != std::string::npos)
		return str.length() - found -1;
	else
		return 1;
}