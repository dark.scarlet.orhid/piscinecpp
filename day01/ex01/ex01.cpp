//
// Created by Anastasiia Trepyton on 5/4/17.
//
#include <iostream>
void memoryLeak()
{
	std::string*        panthere = new std::string("String panthere");
	std::cout << *panthere << std::endl;
	delete panthere;
}