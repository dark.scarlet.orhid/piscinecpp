#include "HumanA.hpp"
#include <iostream>

HumanA::HumanA(std::string n, Weapon& w) : name(n), death(w)
{
	return;
}

HumanA::~HumanA()
{
	return;
}

void HumanA::attack(void)
{
	std::cout << this->name << " attacks with his " << this->death.type << std::endl;
}