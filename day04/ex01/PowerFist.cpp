#include "PowerFist.hpp"
#include <iostream>

PowerFist::PowerFist() : AWeapon("Power Fist", 8, 50)
{ return ;}

PowerFist::PowerFist(const PowerFist &obj) : AWeapon(obj)
{ return ;}

PowerFist::~PowerFist()
{ return; }

void PowerFist::attack() const
{
	std::cout << "* pschhh... SBAM! *" << std::endl;
}