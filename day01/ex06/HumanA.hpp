#include "Weapon.hpp"
#include <string>

class HumanA
{
	public:
		Weapon &death;
		std::string name;
		HumanA(std::string n, Weapon& w);
		~HumanA();
		void attack(void);
};
