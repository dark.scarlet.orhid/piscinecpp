//
// Created by Anastasiia Trepyton on 5/11/17.
//

#ifndef NEWCPP_TACTICALMARINE_HPP
#define NEWCPP_TACTICALMARINE_HPP

#include "ISpaceMarine.hpp"

class TacticalMarine : public ISpaceMarine
{
public:

	TacticalMarine();

	TacticalMarine(TacticalMarine const & obj);

	virtual ~TacticalMarine();

	void operator=(TacticalMarine const & obj);

	ISpaceMarine *clone() const;

	void battleCry() const;

	void rangedAttack() const;

	void meleeAttack() const;
};


#endif
