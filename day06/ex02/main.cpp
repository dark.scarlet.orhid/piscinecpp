//
// Created by Anastasiia Trepyton on 11/5/17.
//

#include <cstdlib>
#include <ctime>
#include <iostream>
#include "Base.hpp"
#include "A.hpp"
#include "B.hpp"
#include "C.hpp"

Base * generate(void)
{
    int rnd = rand() % 3;
    Base *instance = nullptr;
    if (rnd == 0)
    {
        instance = new A();
    }
    else if (rnd == 1)
    {
        instance = new B();
    }
    else if (rnd == 2)
    {
        instance = new C();
    }
    return instance;
}

void identify_from_pointer( Base * p )
{
    A *a = dynamic_cast<A*>(p);
    B *b = dynamic_cast<B*>(p);
    C *c = dynamic_cast<C*>(p);
    if (a) {
        std::cout << "A" << std::endl;
    } else if (b){
        std::cout << "B" << std::endl;
    } else if (c){
        std::cout << "C" << std::endl;
    }
}

void identify_from_reference( Base & p )
{
    try {
        A& a = dynamic_cast<A&>(p);
        std::cout << "A" << std::endl;
    }
    catch (std::bad_cast &bad_cast)
    {
        std::cout << bad_cast.what() << std::endl;
    }

    try {
        B& b = dynamic_cast<B&>(p);
        std::cout << "B" << std::endl;
    }
    catch (std::bad_cast &bad_cast)
    {
        std::cout << bad_cast.what() << std::endl;
    }

    try {
        C& c = dynamic_cast<C&>(p);
        std::cout << "C" << std::endl;
    }
    catch (std::bad_cast &bad_cast)
    {
        std::cout << bad_cast.what() << std::endl;
    }
}

int main()
{
    srand(time(NULL));
    Base *test1 = generate();
    Base *test2 = generate();
    Base *test3 = generate();
    Base *test4 = generate();
    Base *test5 = generate();

    std::cout << "Test identify from pointer\n\n";
    identify_from_pointer(test1);
    identify_from_pointer(test2);
    identify_from_pointer(test3);
    identify_from_pointer(test4);
    identify_from_pointer(test5);

    std::cout << "Test identify from reference\n\n";
    identify_from_reference(*test1);
    identify_from_reference(*test2);
    identify_from_reference(*test3);
    identify_from_reference(*test4);
    identify_from_reference(*test5);
    return 0;
}