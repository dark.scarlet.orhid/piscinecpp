#include <iostream>
#include <sstream>     
#include <string>
class EvalExpr
{
private:
    std::string _expression;
public:
    EvalExpr();
    EvalExpr(std::string expression);
	EvalExpr(const Fixed &obj);
	~EvalExpr();
};

std::ostream& operator<<(std::ostream& os, EvalExpr const & obj);

