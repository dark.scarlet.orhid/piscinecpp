//
// Created by Anastasiia Trepyton on 11/5/17.
//
#include "Array.hpp"
#include <string>
#include <iostream> 
int main()
{
    Array<int> test(5);

    for (int i=0; i < 5; i++)
    {
        test[i] = i;
    }

    for (int i=0; i < 5; i++)
    {
        std::cout << test[i] << std::endl;
    }


    Array<std::string> test2(5);
    
        for (int i=0; i < 5; i++)
        {
            test2[i] = "string";
        }
    
        for (int i=0; i < 5; i++)
        {
            std::cout << test2[i] << std::endl;
        }
    return 0;
}