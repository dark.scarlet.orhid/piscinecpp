//
// Created by Anastasiia Trepyton on 11/4/17.
//

#ifndef DAY05_OFFICEBLOCK_HPP
#define DAY05_OFFICEBLOCK_HPP

#include "Intern.hpp"

class OfficeBlock
{
private:
    Intern *intern;
    Bureaucrat *executor;
    Bureaucrat *signer;

    OfficeBlock & operator=(OfficeBlock const & rhs);
    OfficeBlock(OfficeBlock const & src);
public:
    void setIntern(Intern &intern);

    void setExecutor(Bureaucrat &executor);

    void setSigner(Bureaucrat &signer);

    OfficeBlock();

    OfficeBlock(Intern *intern, Bureaucrat *executor, Bureaucrat *signer);

    virtual ~OfficeBlock();

    void doBureaucracy(std::string formName, std::string target);

    struct InternNotAtWork : public std::exception
    {
        virtual const char * what() const throw();
    };

    struct ExecutingBureaucratNotAtWork : public std::exception
    {
        virtual const char * what() const throw();
    };

    struct SigningBureaucratNotAtWork : public std::exception
    {
        virtual const char * what() const throw();
    };
};
#endif //DAY05_OFFICEBLOCK_HPP
