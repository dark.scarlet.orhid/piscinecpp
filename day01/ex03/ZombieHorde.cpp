#include "ZombieHorde.hpp"
ZombieHorde::ZombieHorde(int n)
{
	srand (time(NULL));
	std::string name[10] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
	this->horde = new Zombie[n];
	for (int i = 0; i < n; i++)
	{
		this->horde[i].name = name[rand() % 10];
		this->horde[i].type = "Spitter";
	}
	return;
}

ZombieHorde::~ZombieHorde()
{
	return;
}

void	ZombieHorde::announce()
{
	for(int i = 0; i < sizeof(this->horde); i++)
	{
		this->horde[i].announce();
	}
}