//
// Created by Anastasiia Trepyton on 5/10/17.
//

#ifndef NEWCPP_CHARACTER_HPP
#define NEWCPP_CHARACTER_HPP

#include "AWeapon.hpp"
#include "Enemy.hpp"
#include <string>
#include <ostream>

class Character
{
private:
	std::string _name;
	int _ap;
	AWeapon *_weapon;
public:
	Character(std::string const &name);
	Character();
	Character(Character const & obj);
	virtual ~Character();
	void operator=(Character const & obj);
	void recoverAP();

	void equip(AWeapon *);

	void attack(Enemy *);

	int get_ap() const;

	AWeapon *get_weapon() const;

	std::string const &	getName() const;


};
std::ostream & operator<<(std::ostream &os, const Character &character);
#endif //NEWCPP_CHARACTER_HPP
