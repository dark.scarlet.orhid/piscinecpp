#include <string>
#include <iostream>

class Zombie
{
	public:
		std::string name;
		std::string type;
		Zombie();
		~Zombie();
		void announce();

};
