
#include <iostream>
#include <fstream>
#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm() : Form()
{
    return;
}

ShrubberyCreationForm::~ShrubberyCreationForm() {
    return;
}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const &obj) {
    *this = obj;
}

ShrubberyCreationForm &ShrubberyCreationForm::operator=(ShrubberyCreationForm const &rhs) {
    if (&rhs != this)
        return *this;
    return *this;
}

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : Form("ShrubberyCreationForm", 145, 137, target){
    return ;
}

void ShrubberyCreationForm::execute(Bureaucrat const &executor) const {

    try {
        if (this->isSign() && executor.getGrade() <= this->getGrade_execute()) {

            std::ofstream outfile(this->getTarget() + "_shrubbery");

            outfile << "           \\/ |    |/\n"
                    "        \\/ / \\||/  /_/___/_\n"
                    "         \\/   |/ \\/\n"
                    "    _\\__\\_\\   |  /_____/_\n"
                    "           \\  | /          /\n"
                    "  __ _-----`  |{,-----------~\n"
                    "            \\ }{\n"
                    "             }{{\n"
                    "             }}{\n"
                    "             {{}\n"
                    "       , -=-~{ .-^- _\n"
                    "             `}\n"
                    "              {\n"
                    " " << std::endl;

            outfile.close();
        }
        else{
            if (this->isSign()) {
                throw Bureaucrat::GradeTooLowException();
            } else if (executor.getGrade() > this->getGrade_execute()) {
                throw Form::FormIsNorSignedException();
            }
        }
    }
    catch (std::exception & e)
    {
        std::cout << "Exception occurred: " << e.what() << std::endl;
    }
}

