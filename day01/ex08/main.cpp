#include "Human.hpp"
int main()
{
	Human creature;

	creature.action("meleeAttack", "pigeon");
	creature.action("rangedAttack", "goat");
	creature.action("intimidatingShout", "crocodile");
	return 0;
}