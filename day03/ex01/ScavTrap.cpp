#include "ScavTrap.hpp"
#include <iostream>
#include <string>

ScavTrap::ScavTrap() {return;}

ScavTrap::ScavTrap(const std::string name)
{
	srand(time(0));
	this->name = name;
	this->hitPoints = 100;
	this->maxHitPoints = 100;
	this->energyPoints = 50;
	this->maxEnergyPoints = 50;
	this->level = 1;
	this->meleeAttackDamage = 20;
	this->rangedAttackDamage = 15;
	this->armorDamageReduction = 3;
	std::cout << "ScavTrap " << this->name << " constructed" << std::endl;
	return;
}

ScavTrap::~ScavTrap() {
	std::cout << "ScavTrap " << this->name << " destructed"<< std::endl;
	return;
}

ScavTrap::ScavTrap(const ScavTrap &rhs)
{
	*this = rhs;
	return;
}

void ScavTrap::operator=(const ScavTrap & rhs)
{
	this->name = rhs.getName();
	this->hitPoints = rhs.getHitPoints();
	this->maxHitPoints = rhs.getMaxHitPoints();
	this->energyPoints = rhs.getEnergyPoints();
	this->maxEnergyPoints = rhs.getMaxEnergyPoints();
	this->level = getLevel();
	this->meleeAttackDamage = getMeleeAttackDamage();
	this->rangedAttackDamage = getRangedAttackDamage();
	this->armorDamageReduction = getArmorDamageReduction();
}

int ScavTrap::getHitPoints() const { return hitPoints; }

int ScavTrap::getMaxHitPoints() const { return maxHitPoints; }

int ScavTrap::getEnergyPoints() const { return energyPoints; }

int ScavTrap::getMaxEnergyPoints() const { return maxEnergyPoints; }

int ScavTrap::getLevel() const { return level; }

int ScavTrap::getMeleeAttackDamage() const { return meleeAttackDamage; }

int ScavTrap::getRangedAttackDamage() const { return rangedAttackDamage; }

int ScavTrap::getArmorDamageReduction() const { return armorDamageReduction; }

const std::string &ScavTrap::getName() const { return name; }

void ScavTrap::rangedAttack(std::string const & target)
{
	if (this->getHitPoints() >= this->getRangedAttackDamage())
	{
		std::cout << "ScavTrap " << this->getName() << " attacks " << target
				  << " at range, causing " << this->getRangedAttackDamage()
				  << " points of damage !" << std::endl;
		this->takeDamage(this->getRangedAttackDamage());
	}
	else
		std::cout << "Not enough points to do ranged attack." << std::endl;
}

void ScavTrap::meleeAttack(std::string const & target)
{
	if (this->getHitPoints() >= this->getMeleeAttackDamage())
	{
		std::cout << "ScavTrap " << this->getName()  << " attacks " << target
				  << " at melee, causing " << this->getMeleeAttackDamage()
				  << " points of damage !" << std::endl;
		this->takeDamage(this->getMeleeAttackDamage());
	}
	else
		std::cout << "Not enough points to do melee attack." << std::endl;
}

void ScavTrap::takeDamage(unsigned int amount)
{
	if (this->getHitPoints() - amount + this->getArmorDamageReduction() > 0)
	{
		this->hitPoints = this->getHitPoints() - amount + this->getArmorDamageReduction();
		std::cout << "ScavTrap "<< this->getName()  <<" lost "
				  << amount - this->getArmorDamageReduction()
				  << " points"<< std::endl;
	}
	else
	{
		std::cout << amount << " points can't be taken."<< std::endl;
	}
}

void ScavTrap::beRepaired(unsigned int amount)
{
	if (this->getHitPoints() + amount <= this->getMaxHitPoints())
	{
		this->hitPoints= this->getHitPoints() + amount;
		std::cout << "ScavTrap was repaired with " << amount
				  << " points"<< std::endl;
	}
	else
	{
		std::cout << "ScavTrap "<< this->getName()
				  << " can't be repaired. Amount exceeds max number of points"
				  << std::endl;
	}
}

void ScavTrap::challengeNewcomer()
{
	std::string ch1 = "Close vim";
	std::string ch2 = "Write ft_ls in one day";
	std::string ch3 = "Write RT in one 25-row function";
	std::string ch4 = "Explain what 'const' does in C++";
	std::string ch5 = "Validate PHP piscine";
	std::string ch6 = "Understand the pdf of this task";
	std::string challenges[6] = {ch1, ch2, ch3, ch4, ch5, ch6};
	std::cout << "ScavTrap " << this->name << " challenges you with the following challenge: "
			  << challenges[rand() % 6] <<std::endl;
}