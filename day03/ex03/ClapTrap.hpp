#ifndef CLAPTRAP_HPP
#define CLAPTRAP_HPP
#include <string>

class ClapTrap
{
public:
	ClapTrap();
	ClapTrap(std::string name);
	~ClapTrap();
	ClapTrap(const ClapTrap & rhs);
	void operator=(const ClapTrap & rhs);
	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);

	int getHitPoints() const;
	int getMaxHitPoints() const;
	int getEnergyPoints() const;
	int getMaxEnergyPoints() const;
	int getLevel() const;
	int getMeleeAttackDamage() const;
	int getRangedAttackDamage() const;
	int getArmorDamageReduction() const;
	const std::string &getName() const;

protected:
	int hitPoints;
	int maxHitPoints;
	int energyPoints;
	int maxEnergyPoints;
	int level;
	std::string name;
	std::string type;
	int meleeAttackDamage;
	int rangedAttackDamage;
	int armorDamageReduction;

};

#endif