//
// Created by Anastasiia Trepyton on 5/6/17.
//
#include <fstream>
#include <iostream>
#include <string>
int main(int argc, char **argv)
{
	if (argc == 1)
	{
		std::string input;
		while (getline(std::cin, input))
			std::cout << input << std::endl;
	}
	else
	{
		std::fstream file;

			for (int i = 1; i < argc; i++)
			{
				file.open(argv[i]);
				if (!file.is_open())
				{
					std::cout << "cato9tails: " << argv[i]
							  << ": No such file or directory" << std::endl;
				}
				else
				{
					file.seekg(0, file.end);
					int length = file.tellg();
					file.seekg(0, file.beg);
					char *line = new char[length];
					while (file.read(line, length))
						std::cout << line;
					file.close();
				}
		}
	}
}