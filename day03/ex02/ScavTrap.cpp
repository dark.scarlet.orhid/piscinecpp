#include "ScavTrap.hpp"
#include <iostream>
#include <string>

ScavTrap::ScavTrap() {return;}

ScavTrap::ScavTrap(const std::string name)
{
	this->name = name;
	this->type = "ScavTrap";
	this->hitPoints = 100;
	this->maxHitPoints = 100;
	this->energyPoints = 50;
	this->maxEnergyPoints = 50;
	this->level = 1;
	this->meleeAttackDamage = 20;
	this->rangedAttackDamage = 15;
	this->armorDamageReduction = 3;
	std::cout << "ScavTrap " << this->name << " constructed" << std::endl;
	return;
}

ScavTrap::~ScavTrap() {
	std::cout << "ScavTrap " << this->name << " destructed"<< std::endl;
	return;
}

ScavTrap::ScavTrap(const ScavTrap &rhs)
{
	*this = rhs;
	return;
}

void ScavTrap::challengeNewcomer()
{
	std::string ch1 = "Close vim";
	std::string ch2 = "Write ft_ls in one day";
	std::string ch3 = "Write RT in one 25-row function";
	std::string ch4 = "Explain what 'const' does in C++";
	std::string ch5 = "Validate PHP piscine";
	std::string ch6 = "Understand the pdf of this task";
	std::string challenges[6] = {ch1, ch2, ch3, ch4, ch5, ch6};
	std::cout << "ScavTrap " << this->name << " challenges you with the following challenge: "
			  << challenges[rand() % 6] <<std::endl;
}