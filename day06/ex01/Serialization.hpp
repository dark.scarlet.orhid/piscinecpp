//
// Created by Anastasiia Trepyton on 11/5/17.
//

#ifndef DAY06_SERIALIZATION_HPP
#define DAY06_SERIALIZATION_HPP


#include <string>

class Serialization {

public:
    struct Data {
        std::string s1;
        int n;
        std::string s2;
    } typedef Data;

    Serialization();
    Serialization(const Serialization & obj);
    virtual ~Serialization();
    Serialization &operator=(const Serialization & obj);

    void * serialize( void );
    Data * deserialize( void * raw );

private:
    const std::string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefgihjklmnopqrstuvwxyz0123456789";
    std::string generateAlphaNumericalString();
};


#endif //DAY06_SERIALIZATION_HPP
