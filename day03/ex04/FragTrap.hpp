#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP
#include <string>
#include "ClapTrap.hpp"

class FragTrap : public virtual ClapTrap
{
	public:
		FragTrap();
		FragTrap(const std::string name);
		~FragTrap();
		FragTrap(const FragTrap & rhs);
		void vaulthunter_dot_exe(std::string const & target);

		void meleeAttack(std::string const & target);
		void rangedAttack(std::string const & target);

	private:
		void conditionerAttack(std::string const & target);
		void segfaultAttack(std::string const & target);
		void busErrorAttack(std::string const & target);
		void vimAttack(std::string const & target);
		void emacsAttack(std::string const & target);
};

#endif