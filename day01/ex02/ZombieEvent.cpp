#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent()
{
	return;
}

ZombieEvent::~ZombieEvent()
{
	return;
}

void ZombieEvent::setZombieType(std::string t)
{
	this->type = t;
}

Zombie* ZombieEvent::newZombie(std::string name)
{
	Zombie *z = new Zombie();
	z->name = name;
	z->type = this->type;
	return z;
}

