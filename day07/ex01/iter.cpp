//
// Created by Anastasiia Trepyton on 11/5/17.
//

#include <cstdlib>
#include <iostream>

template<class T>
void iter(T* array, size_t len, void (*func)(T const &))
{
    if (array)
    {
        for (size_t i = 0; i < len; i++)
        {
            func(array[i]);
        }
    }
}

void addOneAndPrintInt(int const & elem)
{
    std::cout << elem + 1;
    std::cout << " ";
}

void addOneAndPrintDouble(double const & elem)
{
    std::cout << elem + 1;
    std::cout << " ";

}

void addOneAndPrintChar(char const & elem)
{
    std::cout << static_cast<char>(elem + 1);
    std::cout << " ";

}

int main()
{

    int arr1[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    char arr2[5] = {'a', 'b', 'c', 'd', 'e'};
    double arr3[3] = {0.15, 25.456, 278.2656};

    iter(arr1, 10, addOneAndPrintInt);
    std::cout << std::endl;
    iter(arr2, 5, addOneAndPrintChar);
    std::cout << std::endl;
    iter(arr3, 3, addOneAndPrintDouble);
    return 0;
}