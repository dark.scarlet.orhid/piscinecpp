//
// Created by Anastasiia Trepyton on 5/10/17.
//

#ifndef PLASMARIFLE_HPP
#define PLASMARIFLE_HPP

#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon
{
	public:
		PlasmaRifle(const PlasmaRifle &obj);

		PlasmaRifle();

		virtual ~PlasmaRifle();

		void attack() const;
};
#endif //NEWCPP_PLASMARIFLE_HPP
