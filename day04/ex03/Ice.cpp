
#include "Ice.hpp"

Ice::Ice() : AMateria("ice"){
    return ;
}

Ice::Ice(const Ice &obj) {
    *this = obj;
}

Ice &Ice::operator=(const Ice &obj) {

    //??????????????????
    return *this;
}

Ice::~Ice() {
    return;
}

AMateria *Ice::clone() const {
    return nullptr;
}

void Ice::use(ICharacter &target) {
    AMateria::use(target);
    std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}
