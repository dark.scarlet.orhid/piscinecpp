//
// Created by Anastasiia Trepyton on 5/10/17.
//

#include "SuperMutant.hpp"
#include <iostream>
SuperMutant::SuperMutant() : Enemy(170, "Super Mutant")
{
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
	return ;
}

SuperMutant::SuperMutant(SuperMutant const & obj) : Enemy(obj)
{ return ;}

SuperMutant::~SuperMutant()
{
	std::cout << "Aaargh ..." << std::endl;
	return ;
}

void SuperMutant::takeDamage(int n)
{
	Enemy::takeDamage(n - 3);
}
