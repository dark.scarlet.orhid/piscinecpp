#include "AssaultTerminator.hpp"

ISpaceMarine *AssaultTerminator::clone() const
{
	ISpaceMarine *a = new AssaultTerminator(*this);
	return a;
}

void AssaultTerminator::battleCry() const
{
	std::cout << "This code is unclean. PURIFY IT !" << std::endl;
}

void AssaultTerminator::rangedAttack() const
{
	std::cout << "* does nothing *" << std::endl;
}

void AssaultTerminator::meleeAttack() const
{
	std::cout << "* attacks with chainfists *" << std::endl;
}

AssaultTerminator::AssaultTerminator()
{
	std::cout << "* teleports from space *" << std::endl;
	return ;
}

AssaultTerminator::~AssaultTerminator()
{
	std::cout << "I'll be back ..." << std::endl;
	return;
}


AssaultTerminator::AssaultTerminator(AssaultTerminator const &obj)
{
	*this = obj;
}

void AssaultTerminator::operator=(AssaultTerminator const &obj)
{
	return;
}
