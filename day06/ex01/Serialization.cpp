//
// Created by Anastasiia Trepyton on 11/5/17.
//

#include <cstdlib>
#include "Serialization.hpp"


void *Serialization::serialize(void) {

    Data *data =  new Data();
    data->s1 = this->generateAlphaNumericalString();
    data->n = rand();
    data->s2 = this->generateAlphaNumericalString();
    return reinterpret_cast<void *>(data);
}

Serialization::Data *Serialization::deserialize(void *raw) {

    return reinterpret_cast<Data *>(raw);
}

Serialization::Serialization()
{
    srand((time(NULL)));

}

std::string Serialization::generateAlphaNumericalString() {

    std::string result = "";

    for (int i = 0; i < 8; i++)
    {
        result += this->chars.substr(rand() % chars.length(), 1);
    }
    return result;
}

Serialization::~Serialization() = default;

Serialization::Serialization(const Serialization &obj)
{
    *this = obj;

}

Serialization &Serialization::operator=(const Serialization &obj)
{
    return *this;
}
