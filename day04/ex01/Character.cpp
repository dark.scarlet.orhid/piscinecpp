//
// Created by Anastasiia Trepyton on 5/10/17.
//

#include "Character.hpp"
#include <iostream>

Character::Character()
{ return ;}

Character::Character(std::string const &name)
{
	this->_name = name;
	this->_ap = 40;
	this->_weapon = 0;
	return ;
}

Character::Character(Character const & obj)
{
	*this = obj;
	return ;
}

Character::~Character()
{ return;}


const std::string &Character::getName() const
{
	return _name;
}

void Character::operator=(Character const & obj)
{
	this->_name = obj.getName();
	this->_weapon = obj.get_weapon();
	this->_ap = obj.get_ap();
}

int Character::get_ap() const
{
	return _ap;
}

AWeapon *Character::get_weapon() const
{
	return _weapon;
}

void Character::recoverAP()
{
	this->_ap += 10;
	this->_ap = this->_ap > 40 ? 40 : this->_ap;
}

void Character::equip(AWeapon *wep)
{
	this->_weapon = wep;
}

void Character::attack(Enemy *ch)
{
	if (this->_ap - this->_weapon->getAPCost() >= 0)
	{
		this->_ap -= this->_weapon->getAPCost();
		std::cout << this->_name
				  << " attacks "
				  << ch->getType()
				  << " with a "
				  << this->_weapon->getName()
				  << std::endl;
		if (this->_weapon != 0)
		{
			this->_weapon->attack();
			ch->takeDamage(this->_weapon->getDamage());
			if (ch->getHP() <= 0)
				delete (ch);
		}
	}
}

std::ostream &operator<<(std::ostream &os, const Character &character)
{
	if (character.get_weapon() != 0)
	{
		os << character.getName() << " has " << character.get_ap()
	   << " AP and wields a " << character.get_weapon()->getName() << std::endl;
	} else {
		os << character.getName() << " has " << character.get_ap()
		   << " AP and is unarmed" << std::endl;
	}
	return os;
}
