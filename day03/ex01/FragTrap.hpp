#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP
#include <string>

class FragTrap
{
public:
	FragTrap();
	FragTrap(const std::string name);
	~FragTrap();
	FragTrap(const FragTrap & rhs);
	void operator=(const FragTrap & rhs);
	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);

	int getHitPoints() const;
	int getMaxHitPoints() const;
	int getEnergyPoints() const;
	int getMaxEnergyPoints() const;
	int getLevel() const;
	int getMeleeAttackDamage() const;
	int getRangedAttackDamage() const;
	int getArmorDamageReduction() const;
	const std::string &getName() const;
	void vaulthunter_dot_exe(std::string const & target);
private:
	int hitPoints;
	int maxHitPoints;
	int energyPoints;
	int maxEnergyPoints;
	int level;
	std::string name;
	int meleeAttackDamage;
	int rangedAttackDamage;
	int armorDamageReduction;

	void conditionerAttack(std::string const & target);
	void segfaultAttack(std::string const & target);
	void busErrorAttack(std::string const & target);
	void vimAttack(std::string const & target);
	void emacsAttack(std::string const & target);
};

#endif