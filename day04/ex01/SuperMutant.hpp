
//
// Created by Anastasiia Trepyton on 5/10/17.
//

#ifndef NEWCPP_SUPERMUTANT_HPP
#define NEWCPP_SUPERMUTANT_HPP


#include "Enemy.hpp"

class SuperMutant : public Enemy
{
public:
	SuperMutant();

	virtual ~SuperMutant();

	SuperMutant(SuperMutant const & obj);

	void takeDamage(int);
};



#endif //NEWCPP_SUPERMUTANT_HPP
