#include <exception>
#include <string>
#include <limits>
#include <iostream>
#include <cmath>
#include <iomanip>

class Convertions
{
public:
	Convertions();
	Convertions(Convertions const & obj);
	Convertions(std::string str);
	void operator=(Convertions const & obj);
	~Convertions();

	struct Impossible : public std::exception
	{
		 virtual const char * what() const throw();
	};
	struct NonDisplayable : public std::exception
	{
		 virtual const char * what() const throw();
	};

	void convToChar();
	void convToInt();
	void convToDouble ();
	void convToFloat();



private:
	double toConvert;
	int pres;

	size_t getPoint(std::string str);
};