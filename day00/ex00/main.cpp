#include <string>
#include <iostream>

std::string strtoupper(std::string str)
{
	for (int i = 0; i < (int)str.length(); i++)
	{	
		str[i] = (char)std::toupper(str[i]);
	}	
	return str;
}

int main(int argc, char **argv)
{
	std::string s;
	if (argc == 1)
	{
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *\n";
	}	
	else
	{
		for (int i = 1; i < argc; i++)
		{
			s = argv[i];
			std::cout << strtoupper(s);
		}
		std::cout << std::endl;
	}
}
