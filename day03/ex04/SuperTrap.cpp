#include "SuperTrap.hpp"
#include <string>
#include <iostream>

Supertrap::Supertrap() : NinjaTrap(), FragTrap()  
{
	this->name = "Default";
	this->hitPoints = 100;
	this->maxHitPoints = 100;
	this->energyPoints = 120;
	this->maxEnergyPoints = 120;
	this->level = 1;
	this->meleeAttackDamage = 60;
	this->rangedAttackDamage = 20;
	this->armorDamageReduction = 5;
	std::cout << "SuperTrap " << " constructed" << std::endl;
	return;
}

Supertrap::Supertrap(std::string name) :  NinjaTrap(name), FragTrap(name)	
{
	this->name = name;
	this->hitPoints = 100;
	this->maxHitPoints = 100;
	this->energyPoints = 120;
	this->maxEnergyPoints = 120;
	this->level = 1;
	this->meleeAttackDamage = 60;
	this->rangedAttackDamage = 20;
	this->armorDamageReduction = 5;
	return;
}

Supertrap::~Supertrap()
{
	std::cout << "SuperTrap " << this->name << " destructed" << std::endl;
	return;
}

Supertrap::Supertrap(Supertrap &obj)
{
	*this = obj;
	return;
}

void Supertrap::meleeAttack(const std::string &target) 
{
	NinjaTrap::meleeAttack(target);
}
void Supertrap::rangedAttack(const std::string &target)
{
	 FragTrap::rangedAttack(target);
}