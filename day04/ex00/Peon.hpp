#ifndef PEON_HPP
#define PEON_HPP

#include "Victim.hpp"
#include <string>
class Peon : public Victim
{
public:
	Peon();
	Peon(std::string name);
	Peon(Peon const &obj);
	virtual ~Peon();
	void operator=(const Peon & obj);

	void getPolymorphed() const;
};

std::ostream& operator<<(std::ostream& os, Peon const & obj);

#endif