#ifndef PHONEBOOK_HPP
# define PHONEBOOK_HPP

#include <iostream>
#include <string>
#include "Contact.hpp"
#include <iomanip>
#include <cstdlib>
#include <iostream>

class PhoneBook {
private:
        Contact elements[8];
        int numberOfContacts;
        void searchContactByIndex();
        std::string formatContactOutput(std::string toFormat);
public:
	PhoneBook(void);
	~PhoneBook(void);
    void addContact();
    void displayContacts();
    void exitPhonebook();
    void openPhonebook();
};
#endif