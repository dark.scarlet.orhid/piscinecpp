#ifndef SCATRAP_HPP
# define SCATRAP_HPP
# include <string>

class ScavTrap
{
public:
	ScavTrap();
	ScavTrap(const std::string name);
	~ScavTrap();
	ScavTrap(const ScavTrap & rhs);
	void operator=(const ScavTrap & rhs);
	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);

	int getHitPoints() const;
	int getMaxHitPoints() const;
	int getEnergyPoints() const;
	int getMaxEnergyPoints() const;
	int getLevel() const;
	int getMeleeAttackDamage() const;
	int getRangedAttackDamage() const;
	int getArmorDamageReduction() const;
	const std::string &getName() const;
	void challengeNewcomer();

private:
	int hitPoints;
	int maxHitPoints;
	int energyPoints;
	int maxEnergyPoints;
	int level;
	std::string name;
	int meleeAttackDamage;
	int rangedAttackDamage;
	int armorDamageReduction;

};

#endif