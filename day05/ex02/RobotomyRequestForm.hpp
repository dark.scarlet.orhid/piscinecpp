//
// Created by Anastasiia Trepyton on 11/4/17.
//

#ifndef DAY05_ROBOTOMYREQUESTFORM_HPP
#define DAY05_ROBOTOMYREQUESTFORM_HPP
#include "Form.hpp"

class RobotomyRequestForm : public Form 
{
public:
    RobotomyRequestForm();

    void execute(Bureaucrat const &executor) const;

    RobotomyRequestForm(std::string target);
    RobotomyRequestForm( RobotomyRequestForm const & obj);
    RobotomyRequestForm & operator=(RobotomyRequestForm const & rhs);
    virtual ~RobotomyRequestForm();

private:
    static int switchResult;
    
};
#endif //DAY05_ROBOTOMYREQUESTFORM_HPP
