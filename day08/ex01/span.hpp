#ifndef SPAN_HPP
#define SPAN_HPP

#include <vector>

class Span
{
public:
    Span();

    virtual ~Span();

    Span(unsigned int _size);

//    Span(std::iterator begin, std::iterator end);

    Span(const Span & obj);

    Span &operator=(const Span & obj);

    void addNumber(unsigned int nb);

    unsigned int shortestSpan();

    unsigned int longestSpan();
    //exceptions

    class ExceedSizeOfContainerException : std::exceptions
    {
        ExceedSizeOfContainerException() throw();
        ExceedSizeOfContainerException (const ExceedSizeOfContainerException&) throw();
        ExceedSizeOfContainerException& operator= (const ExceedSizeOfContainerException&) throw();
        virtual ~ExceedSizeOfContainerException() throw();
        const char* what() const throw();
    }

    class EmptyContainerException : std::exceptions
    {
        EmptyContainerException() throw();
        EmptyContainerException (const EmptyContainerException&) throw();
        EmptyContainerException& operator= (const EmptyContainerException&) throw();
        virtual ~EmptyContainerException() throw();
        const char* what() const throw();
    }

    class NotEnoughNumbersInContainerException : std::exceptions
    {
        NotEnoughNumbersInContainerException() throw();
        NotEnoughNumbersInContainerException (const NotEnoughNumbersInContainerException&) throw();
        NotEnoughNumbersInContainerException& operator= (const NotEnoughNumbersInContainerException&) throw();
        virtual ~NotEnoughNumbersInContainerException() throw();
        const char* what() const throw();
    }

private:
    std::vector<unsigned int> _span;

    unsigned int _size;
};
#endif
