#ifndef SORCERER_HPP
#define SORCERER_HPP
#include <string>
#include "Victim.hpp"
class Sorcerer
{
public:
	Sorcerer();
	Sorcerer(std::string name, std::string title);
	Sorcerer(Sorcerer const &obj);
	virtual ~Sorcerer();
	void operator=(const Sorcerer & obj);

	const std::string &get_name() const;
	const std::string &get_title() const;

	virtual void polymorph(Victim const & obj) const;

private:
	std::string _name;
	std::string _title;
};

std::ostream& operator<<(std::ostream& os, Sorcerer const & obj);

#endif