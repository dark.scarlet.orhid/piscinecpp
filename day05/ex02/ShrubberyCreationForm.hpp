//
// Created by Anastasiia Trepyton on 11/4/17.
//

#ifndef DAY05_SHRUBBERYCREATIONFORM_HPP
#define DAY05_SHRUBBERYCREATIONFORM_HPP

#include <ostream>
#include "Form.hpp"

class ShrubberyCreationForm : public Form
{
public:
    ShrubberyCreationForm();
    ShrubberyCreationForm(std::string target);
    ShrubberyCreationForm( ShrubberyCreationForm const & obj);
    ShrubberyCreationForm & operator=(ShrubberyCreationForm const & rhs);
    virtual ~ShrubberyCreationForm();

    void execute(Bureaucrat const &executor) const;

private:
};
#endif //DAY05_SHRUBBERYCREATIONFORM_HPP
