#include "Zombie.hpp"

Zombie::Zombie()
{
	this->name = "default_name";
	this->type = "default_type";
	return;
}

Zombie::~Zombie()
{
	return;
}

void Zombie::announce()
{
	std::cout <<"<" << this->name << " (" << this->type
			  << ")> Braiiiiiiinnnssss..." << std::endl;
}

