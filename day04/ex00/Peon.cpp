#include "Peon.hpp"
#include <iostream>

Peon::Peon() { return; }

Peon::Peon(std::string name) : Victim(name)
{
	std::cout << "Zog zog."
			  << std::endl;
	return;
}

Peon::~Peon()
{
	std::cout << "Bleuark..."
			  << std::endl;
	return;
}

Peon::Peon(Peon const & obj)
{
	*this = obj;
}

void Peon::operator=(const Peon & obj)
{
	this->_name = obj.get_name();
}

void Peon::getPolymorphed() const
{
	std::cout << this->_name
			  << " has been turned into a pink pony !"
			  << std::endl;
}

std::ostream& operator<<(std::ostream& os, Peon const & obj)
{
	os << "I'm "
	   << obj.get_name()
	   << " and I like otters !"
	   << std::endl;
	return os;
}