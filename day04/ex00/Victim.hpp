#ifndef VICTIM_HPP
# define VICTIM_HPP
# include <string>

class Victim
{
public:
	Victim();
	Victim(Victim const &obj);
	Victim(std::string name);
	virtual ~Victim();
	void operator=(const Victim & obj);

	const std::string &get_name() const;
	virtual void getPolymorphed() const;

protected:
	std::string _name;
};

std::ostream& operator<<(std::ostream& os, Victim const & obj);
#endif