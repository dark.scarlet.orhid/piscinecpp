#include "Contact.hpp"
#include <iostream>
#include <iomanip>
#include <string>

Contact::Contact()
{
	return;
}

Contact::~Contact()
{
	return;
}

void Contact::setFirstName(std::string name)
{
	this->first_name = name;
}

void Contact::setLastName(std::string name)
{
	this->last_name = name;
}

void Contact::setNickname(std::string name)
{
	this->nickname = name;
}

void Contact::setLogin(std::string name)
{
	this->login = name;
}

void Contact::setAddress(std::string name)
{
	this->address = name;
}

void Contact::setEmail(std::string name)
{
	this->email = name;
}

void Contact::setPhoneNumber(std::string name)
{
	this->phone_number = name;
}

void Contact::setBirthday(std::string name)
{
	this->birthday = name;
}

void Contact::setFavouriteMeal(std::string name)
{
	this->favorite_meal = name;
}

void Contact::setUnderwearColor(std::string name)
{
	this->underwear_color = name;
}

void Contact::setDarkestSecret(std::string name)
{
	this->darkest_secret = name;
}

const std::string &Contact::getFirst_name() const
{
	return first_name;
}

const std::string &Contact::getLast_name() const
{
	return last_name;
}

const std::string &Contact::getNickname() const
{
	return nickname;
}

const std::string &Contact::getLogin() const
{
	return login;
}

const std::string &Contact::getAddress() const
{
	return address;
}

const std::string &Contact::getEmail() const
{
	return email;
}

const std::string &Contact::getPhone_number() const
{
	return phone_number;
}

const std::string &Contact::getBirthday() const
{
	return birthday;
}

const std::string &Contact::getFavorite_meal() const
{
	return favorite_meal;
}

const std::string &Contact::getUnderwear_color() const
{
	return underwear_color;
}

const std::string &Contact::getDarkest_secret() const
{
	return darkest_secret;
}

void Contact::toString()
{
	std::cout << getFirst_name() << std::endl;
	std::cout << getLast_name() << std::endl;
	std::cout << getNickname() << std::endl;
	std::cout << getLogin() << std::endl;
	std::cout << getAddress() << std::endl;
	std::cout << getEmail() << std::endl;
	std::cout << getPhone_number() << std::endl;
	std::cout << getBirthday() << std::endl;
	std::cout << getFavorite_meal() << std::endl;
	std::cout << getUnderwear_color() << std::endl;
	std::cout << getDarkest_secret() << std::endl;

}



Contact Contact::receiveContactInfo()
{
	Contact elem;
	std::string inp;

	std::cout << "Enter your first name:\n";
	std::getline(std::cin, inp);
	elem.setFirstName(inp);
	std::cout << "Enter your last name:\n";
	std::getline(std::cin, inp);
	elem.setLastName(inp);
	std::cout << "Enter your nickname:\n";
	std::getline(std::cin, inp);
	elem.setNickname(inp);
	std::cout << "Enter your login:\n";
	std::getline(std::cin, inp);
	elem.setLogin(inp);
	std::cout << "Enter your postal address:\n";
	std::getline(std::cin, inp);
	elem.setAddress(inp);
	std::cout << "Enter your email:\n";
	std::getline(std::cin, inp);
	elem.setEmail(inp);
	std::cout << "Enter your phone number:\n";
	std::getline(std::cin, inp);
	elem.setPhoneNumber(inp);
	std::cout << "Enter your birthday:\n";
	std::getline(std::cin, inp);
	elem.setBirthday(inp);
	std::cout << "Enter your favourite meal:\n";
	std::getline(std::cin, inp);
	elem.setFavouriteMeal(inp);
	std::cout << "Enter your underwear color:\n";
	std::getline(std::cin, inp);
	elem.setUnderwearColor(inp);
	std::cout << "Enter your darkest secret:\n";
	std::getline(std::cin, inp);
	elem.setDarkestSecret(inp);
	std::cout << "Contact created\n";
	return elem;
}