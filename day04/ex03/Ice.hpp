#include "AMateria.hpp"
#include <iostream>

class Ice : public AMateria
{
public:
    Ice();
    Ice(const Ice &obj);

    AMateria *clone() const override;

    void use(ICharacter &target) override;

    Ice & operator=(const Ice &obj);
    ~Ice();
};