//
// Created by Anastasiia Trepyton on 5/11/17.
//

#ifndef NEWCPP_ASSAULTTERMINATOR_HPP
#define NEWCPP_ASSAULTTERMINATOR_HPP

#include "ISpaceMarine.hpp"

class AssaultTerminator : public ISpaceMarine
{
public:

	AssaultTerminator();

	AssaultTerminator(AssaultTerminator const & obj);

	virtual ~AssaultTerminator();

	void operator=(AssaultTerminator const & obj);

	ISpaceMarine *clone() const;

	void battleCry() const;

	void rangedAttack() const;

	void meleeAttack() const;
};


#endif
