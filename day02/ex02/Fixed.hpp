#include <string>
#include <iostream>
class Fixed
{

private:
	int value;
	static const int bits = 8;
public:
	Fixed();
	Fixed(const Fixed &obj);
	Fixed(const int nb);
	Fixed(const float nb);
	~Fixed();
	void operator=(const Fixed &obj);
	int getRawBits( void ) const;
	void setRawBits( int const raw );

	float toFloat( void ) const;
	int toInt( void ) const;

	bool operator<(const Fixed& obj) const;
	bool operator>(const Fixed& obj) const;
	bool operator>=(const Fixed& obj) const;
	bool operator<=(const Fixed& obj) const;
	bool operator==(const Fixed& obj) const;
	bool operator!=(const Fixed& obj) const;

	Fixed &operator++();
	Fixed operator++(int);
	Fixed &operator--();
	Fixed operator--(int);

	Fixed operator+(Fixed& obj);
	Fixed operator-(Fixed& obj);
	Fixed operator*(const Fixed& obj);
	Fixed operator/(Fixed& obj);
	static const Fixed & min(const Fixed & v1, const Fixed & v2);
	static const Fixed & max(const Fixed & v1, const Fixed & v2);
};

std::ostream& operator<<(std::ostream& os, Fixed const & obj);

