#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"
#include <iostream>
int main()
{
	FragTrap bot("UNIT");
	ScavTrap robot("Challenger");
	bot.meleeAttack("atrepyto");
	robot.rangedAttack("cluster");
	bot.vaulthunter_dot_exe("unit burger");
	bot.vaulthunter_dot_exe("fresh burger");
	bot.vaulthunter_dot_exe("american burger");
	robot.challengeNewcomer();
	robot.challengeNewcomer();
	robot.challengeNewcomer();
	bot.rangedAttack("c++");
	bot.rangedAttack("Python");
	bot.rangedAttack("Ruby");
	bot.rangedAttack("PHP");
	robot.meleeAttack("pigeon");
	bot.rangedAttack("c");
	bot.rangedAttack("Java");
	bot.vaulthunter_dot_exe("smart burger");
	bot.vaulthunter_dot_exe("grill burger");

	std::cout << bot.getHitPoints() << std::endl;
	NinjaTrap n("Lego");
	bot.beRepaired(65);
	bot.rangedAttack("Lego");
	n.ninjaShoebox(bot);
	n.ninjaShoebox(robot);

	Supertrap sup("Myname");
	std::cout << sup.getHitPoints() << std::endl;
	sup.meleeAttack("t1");
	sup.rangedAttack("t2");
	sup.vaulthunter_dot_exe("ray tracer");
	sup.ninjaShoebox(bot);
	return 0;
}
