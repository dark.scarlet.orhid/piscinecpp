#include <fstream>
#include <iostream>
#include <string>
#include "Logger.hpp"

Logger::Logger(std::string file)
{
	this->filename = file;
	return;
}

void Logger::logToConsole(std::string msg)
{
	std::string logmsg = this->makeLogEntry(msg);
	std::cout << logmsg <<std::endl;
}

void Logger::logToFile(std::string msg)
{
	std::fstream file;
	file.open(this->filename, std::fstream::out | std::fstream::app);
	std::string logmsg = this->makeLogEntry(msg);
	file << logmsg << std::endl;
	file.close();
}

std::string Logger::makeLogEntry(std::string message)
{
	std::time_t t = std::time(NULL);
	char str[19];
	std::strftime(str, sizeof(str), "[%Y%d%m_%H%M%S] ", std::localtime(&t));
	std::string ret = str + message;
	return ret;
}

void Logger::log(std::string const & dest, std::string const & message)
{
	void(Logger::*point[2]) (std::string) = {&Logger::logToConsole, &Logger::logToFile};
	std::string dests[2] = {"logToConsole", "logToFile"};
	int i;
	for(i = 0; i < 2; i++)
	{
		if (dest.compare(dests[i]) == 0)
			break;
	}
	if (i < 2)
	{
		(this->*point[i])(message);
	}
}
