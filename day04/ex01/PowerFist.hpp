//
// Created by Anastasiia Trepyton on 5/10/17.
//

#ifndef NEWCPP_POWERFIRST_HPP
#define NEWCPP_POWERFIRST_HPP

#include "AWeapon.hpp"

class PowerFist : public AWeapon
{
	public:
		PowerFist();
		PowerFist(const PowerFist &obj);
		virtual ~PowerFist();

		void attack() const;
};
#endif //NEWCPP_POWERFIRST_HPP
