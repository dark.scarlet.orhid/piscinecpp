
#include <iostream>
#include "Bureaucrat.hpp"
#include "Form.hpp"

int main()
{
	Bureaucrat Petya("Petya", 10);
	std::cout << Petya << std::endl;
	Bureaucrat Vova("Vova", 143);
	std::cout << Vova << std::endl;
	Bureaucrat Kazymyr("Kazymyr", 0);
	std::cout << Kazymyr << std::endl;

//	for(int i = 0; i < 10; i++)
//		Petya.incrementGrade();
//
//	for(int i = 0; i < 150; i++)
//		Petya.decrementGrade();

	std::cout << Petya << std::endl;

	Form forma("POVISTKA U VOIENKOMAT", 11, 150);
	std::cout << forma << std::endl;
	forma.beSigned(Petya);
	forma.beSigned(Vova);
	std::cout << forma << std::endl;


}