#ifndef SUPERTRAP_HPP
# define SUPERTRAP_HPP
# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "NinjaTrap.hpp"

class Supertrap : public FragTrap, public NinjaTrap
{
	public:
		Supertrap();
		Supertrap(std::string name);
		~Supertrap();
		Supertrap(Supertrap &obj);
		void meleeAttack(const std::string &target);
		void rangedAttack(const std::string &target);
};
#endif