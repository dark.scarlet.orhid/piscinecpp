#include <iostream>
#include <string>
#include <cmath>
#include "Fixed.hpp"
Fixed::Fixed()
{
	this->value = 0;
	return;
}
Fixed::~Fixed()
{
	return;
}

Fixed::Fixed(Fixed const &src)
{
	*this = src;
	return;
}

void Fixed::operator=(const Fixed &obj)
{
	this->setRawBits(obj.getRawBits());
}

int Fixed::getRawBits( void ) const
{
	return this->value;
}

void Fixed::setRawBits( int const raw )
{
	this->value = raw;
}

Fixed::Fixed(const int nb)
{
	this->value = (nb * (1 << this->bits));
}

Fixed::Fixed(const float nb)
{
	this->value = roundf(nb * (1 << this->bits));
}

float Fixed::toFloat( void ) const
{
	return this->value / (float)(1 << this->bits);
}

int Fixed::toInt( void ) const
{
	return this->value / (1 << this->bits);
}

std::ostream& operator<<(std::ostream& os, Fixed const & obj)
{
	os << obj.toFloat();
	return os;
}
//-----comparison operators-----------//
bool Fixed::operator<(const Fixed& obj) const
{
	return (this->value < obj.value);
}

bool Fixed::operator>(const Fixed& obj) const
{
	return (this->value > obj.value);
}

bool Fixed::operator>=(const Fixed& obj) const
{
	return (this->value >= obj.value);
}

bool Fixed::operator<=(const Fixed& obj) const
{
	return (this->value <= obj.value);
}

bool Fixed::operator==(const Fixed& obj) const
{
	return (this->value == obj.value);
}

bool Fixed::operator!=(const Fixed& obj) const
{
	return (this->value != obj.value);
}
//-----------------arithmetic operators---------------------//

Fixed Fixed::operator+(Fixed & obj)
{
	Fixed res;
	res.setRawBits(this->value + obj.getRawBits());
	return res;
}

Fixed Fixed::operator-(Fixed & obj)
{
	Fixed res;
	res.setRawBits(this->value - obj.getRawBits());
	return res;
}


Fixed Fixed::operator*(const Fixed & obj)
{
	Fixed res;
	res.setRawBits((this->value * obj.getRawBits()) >> 8);
	return (res);
}

Fixed Fixed::operator/(Fixed & obj)
{
	Fixed res;
	res.setRawBits(this->value / obj.getRawBits());
	return res;
}

Fixed Fixed::operator++(int)
{
	Fixed tmp(*this);
	++(*this);
	return tmp;
}

Fixed & Fixed::operator++()
{
	this->value += 1;
	return *this;
}

Fixed Fixed::operator--(int)
{
	Fixed tmp(*this);
	operator--();
	return tmp;
}

Fixed & Fixed::operator--()
{
	this->value -= 1;
	return *this;
}

const Fixed & Fixed::min(const Fixed & v1, const Fixed & v2)
{
	return v1 < v2 ? v1 : v2;
}

const Fixed & Fixed::max(const Fixed & v1, const Fixed & v2)
{
	return v1 > v2 ? v1 : v2;
}