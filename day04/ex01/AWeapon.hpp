#ifndef AWEAPON_HPP
#define AWEAPON_HPP

#include <string>

class AWeapon
{
	private:
		std::string _name;
		int _damage_points;
		int _shooting_cost;

	public:

		AWeapon(std::string const & name, int apcost, int damage);
		AWeapon();
		AWeapon(AWeapon const & obj);
		void operator=(AWeapon const & obj);
		virtual ~AWeapon();

		std::string const & getName() const;
		int getAPCost() const;
		int getDamage() const;
		virtual void attack() const = 0;
};

#endif //NEWCPP_AWEAPON_HPP
