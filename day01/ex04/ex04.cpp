//
// Created by Anastasiia Trepyton on 5/5/17.
//
#include <string>
#include <iostream>
int main()
{
	std::string brain = "HI THIS IS BRAIN";
	std::string *p_brain = &brain;
	std::string &r_brain = brain;
	std::cout << *p_brain << std::endl;
	std::cout << r_brain << std::endl;
}