#ifndef NINJATRAP_HPP
#define NINJATRAP_HPP
# include "ClapTrap.hpp"
# include "ScavTrap.hpp"
# include "FragTrap.hpp"
# include <string>

class NinjaTrap : public ClapTrap
{
public:
	NinjaTrap();
	NinjaTrap(const std::string name);
	~NinjaTrap();
	NinjaTrap(const NinjaTrap & rhs);
	void ninjaShoebox(FragTrap & obj);
	void ninjaShoebox(ScavTrap & obj);
	void ninjaShoebox(NinjaTrap & obj);
	void ninjaShoebox(ClapTrap & obj);
};
#endif