#include "FragTrap.hpp"
#include <iostream>
int main()
{
	FragTrap bot("UNIT");

	bot.meleeAttack("atrepyto");

	bot.vaulthunter_dot_exe("unit burger");
	bot.vaulthunter_dot_exe("fresh burger");
	bot.vaulthunter_dot_exe("american burger");

	bot.rangedAttack("c++");
	bot.rangedAttack("Python");
	bot.rangedAttack("Ruby");
	bot.rangedAttack("PHP");
	bot.rangedAttack("c");
	bot.rangedAttack("Java");
	bot.vaulthunter_dot_exe("smart burger");
	bot.vaulthunter_dot_exe("grill burger");
	std::cout << bot.getHitPoints() << std::endl;
	return 0;
}
