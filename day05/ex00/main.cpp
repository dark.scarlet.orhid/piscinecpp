//
// Created by Anastasiia Trepyton on 5/17/17.
//


#include <iostream>
#include "Bureaucrat.hpp"

int main()
{
	Bureaucrat Petya("Petya", 10);
	std::cout << Petya << std::endl;
	Bureaucrat Vova("Vova", 165);
	std::cout << Vova << std::endl;
	Bureaucrat Kazymyr("Kazymyr", 0);
	std::cout << Kazymyr << std::endl;

	for(int i = 0; i < 10; i++)
		Petya.incrementGrade();

	for(int i = 0; i < 150; i++)
		Petya.decrementGrade();

	std::cout << Petya << std::endl;

}