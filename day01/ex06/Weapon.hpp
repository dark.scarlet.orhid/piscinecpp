#include <string>

class Weapon
{
	public:
		std::string type;
		Weapon(std::string);
		~Weapon();
		const std::string& getType();
		void setType(std::string);
};