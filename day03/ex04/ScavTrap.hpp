#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP
# include "ClapTrap.hpp"
# include <string>

class ScavTrap : public ClapTrap
{
public:
	ScavTrap();
	ScavTrap(const std::string name);
	~ScavTrap();
	ScavTrap(const ScavTrap & rhs);
	void challengeNewcomer();

	void meleeAttack(std::string const & target);
	void rangedAttack(std::string const & target);
};

#endif