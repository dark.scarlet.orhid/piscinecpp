//
// Created by Anastasiia Trepyton on 11/4/17.
//

#include <iostream>
#include "Intern.hpp"
#include "../ex02/RobotomyRequestForm.hpp"
#include "../ex02/ShrubberyCreationForm.hpp"
#include "../ex02/PresidentialPardonForm.hpp"

Intern::Intern() {}

Intern::~Intern() {

}

Intern::Intern(Intern const &obj) {
    *this = obj;
}

Intern &Intern::operator=(Intern const &obj) {
    if (&obj != this)
        return *this;
    return *this;
}

Form *Intern::makeForm(std::string name, std::string target) {

    try {
        if (name.find("robotomy request") != std::string::npos)
        {
            return new RobotomyRequestForm(target);
        }
        else if (name.find("shrubbery creation") != std::string::npos)
        {
            return new ShrubberyCreationForm(target);
        }
        else if (name.find("presidential pardon") != std::string::npos)
        {
            return new PresidentialPardonForm(target);
        }
        else
        {
            throw Intern::NotValidFormException();
        }
    }
    catch (std::exception & e)
    {
        std::cout << "Exception occurred: " << e.what() << std::endl;
    }
    return nullptr;
}

//__Exceptions__
const char *Intern::NotValidFormException::what() const throw()
{
    return "Form is unavailable";
}