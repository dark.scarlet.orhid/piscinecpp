//
// Created by Anastasiia Trepyton on 5/10/17.
//

#include "AWeapon.hpp"

AWeapon::AWeapon()
{ return ;}

AWeapon::AWeapon(std::string const & name, int apcost, int damage)
{
	this->_name = name;
	this->_damage_points = damage;
	this->_shooting_cost = apcost;
	return;
}

AWeapon::AWeapon(AWeapon const & obj)
{
	*this = obj;
	return;
}

AWeapon::~AWeapon() { return;}

void AWeapon::operator=(AWeapon const & obj)
{
	this->_name = obj.getName();
	this->_damage_points = obj.getDamage();
	this->_shooting_cost = obj.getAPCost();
	return;
}

int AWeapon::getAPCost() const
{
	return _shooting_cost;
}

int AWeapon::getDamage() const
{
	return _damage_points;
}

std::string const & AWeapon::getName() const
{
	return _name;
}