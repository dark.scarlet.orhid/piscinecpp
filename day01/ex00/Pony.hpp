#include <string>

class Pony
{
	public:
		std::string name;
		std::string breed;
		Pony(std::string n, std::string b);
		~Pony(void);
};