#include "Zombie.hpp"
#include <string>
#include <iostream>

class ZombieEvent {

	std::string type;
	public:
		ZombieEvent();
		~ZombieEvent();
		void setZombieType(std::string t);
		Zombie* newZombie(std::string name);
};