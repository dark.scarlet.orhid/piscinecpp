#include "Pony.hpp"

//how to check the heap and stack memory alocation??
Pony *ponyOnTheHeap(void)
{
	Pony *heap = new Pony("Rainbow Dash", "Pegasus");
	return heap;
}

Pony ponyOnTheStack(void)
{
	Pony stack("Twilight", "Unicorn");
	return stack;
}

int main()
{
	Pony p1 = ponyOnTheStack();
	Pony *p2 = ponyOnTheHeap();
	delete(p2);
}
