#include <iostream>
#include <string>
#include <cmath>

#include "Fixed.hpp"
Fixed::Fixed()
{
	this->value = 0;
	std::cout << "Default constructor called\n";
	return;
}
Fixed::~Fixed()
{
	std::cout << "Destructor called\n";
	return;
}

Fixed::Fixed(Fixed const &src)
{
	std::cout << "Copy constructor called\n";
	*this = src;
	return;
}

void Fixed::operator=(const Fixed &obj)
{
	std::cout << "Assignation operator called\n";
	this->setRawBits(obj.getRawBits());
}

int Fixed::getRawBits( void ) const
{
	return this->value;
}

void Fixed::setRawBits( int const raw )
{
	this->value = raw;
}

Fixed::Fixed(const int nb)
{
	std::cout << "Int constructor called\n";
	this->value = (nb * (1 << this->bits));
}

Fixed::Fixed(const float nb)
{
	std::cout << "Float constructor called\n";
	this->value = roundf(nb * (1 << this->bits));
}

float Fixed::toFloat( void ) const
{
	return this->value / (float)(1 << this->bits);
}

int Fixed::toInt( void ) const
{
	return this->value / (1 << this->bits);
}

std::ostream& operator<<(std::ostream& os, Fixed const & obj)
{
	os << obj.toFloat();
	return os;
}
