#include "AMateria.hpp"
#include <iostream>
class Cure : public AMateria
{
public:
    Cure();
    Cure(const Cure &obj);
    Cure & operator=(const Cure &obj);
    ~Cure();

    AMateria *clone() const override;

    void use(ICharacter &target) override;
};