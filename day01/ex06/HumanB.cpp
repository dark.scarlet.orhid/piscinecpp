#include "HumanB.hpp"
#include <iostream>

HumanB::HumanB(std::string n) : name(n)
{
	return;
}

HumanB::~HumanB()
{
	return;
}

void HumanB::setWeapon(Weapon& w)
{
	this->death = &w;
}

void HumanB::attack(void)
{
	std::cout << this->name << " attacks with his " << this->death->type << std::endl;
}