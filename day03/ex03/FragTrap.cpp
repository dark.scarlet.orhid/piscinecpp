#include "FragTrap.hpp"
#include <string>
#include <iostream>
#include <ctime>

FragTrap::FragTrap(){return;}

FragTrap::FragTrap(const std::string name)
{
	this->name = name;
	this->type = "R4G-TP";
	this->hitPoints = 100;
	this->maxHitPoints = 100;
	this->energyPoints = 100;
	this->level = 1;
	this->maxEnergyPoints = 100;
	this->meleeAttackDamage = 30;
	this->rangedAttackDamage = 20;
	this->armorDamageReduction = 5;
	std::cout << "FR4G-TP " << this->name << " constructed" << std::endl;
	return;
}

FragTrap::~FragTrap() {
	std::cout << "FR4G-TP " << this->name << " destructed"<< std::endl;
	return;
}

FragTrap::FragTrap(const FragTrap &rhs)
{
	*this = rhs;
	return;
}

void FragTrap::conditionerAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at conditioner, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP " << this->getName()  <<" is out of energy." << std::endl;
	}
}

void FragTrap::segfaultAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at segfault, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()  <<" is out of energy." << std::endl;
	}
}

void FragTrap::busErrorAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at bus error, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()  <<" is out of energy." << std::endl;
	}
}

void FragTrap::vimAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at vim, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()  <<" is out of energy."
				  << std::endl;
	}
}

void FragTrap::emacsAttack(std::string const & target)
{
	if (this->energyPoints >= 25)
	{
		this->energyPoints -= 25;
		std::cout << "FR4G-TP " << this->getName()  << " attacks " << target
				  << " at emacs, loosing 25 energy points !" << std::endl;
	}
	else
	{
		std::cout << "FR4G-TP "<< this->getName()  << " is out of energy."
				  << std::endl;
	}
}

void FragTrap::vaulthunter_dot_exe(std::string const & target)
{
	void(FragTrap::*point[5]) (std::string const &) = {&FragTrap::conditionerAttack,
	&FragTrap::segfaultAttack, &FragTrap::busErrorAttack, &FragTrap::vimAttack,
	&FragTrap::emacsAttack};
	(this->*point[rand() % 5])(target);
}
