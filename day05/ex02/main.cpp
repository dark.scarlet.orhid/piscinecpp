
#include <iostream>
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

int main()
{
	Bureaucrat b1("Petya", 4);
	Bureaucrat b2("Serge", 48);
	Bureaucrat b3("Vitalik", 150);

    ShrubberyCreationForm form1("unit");
    RobotomyRequestForm form2("piscine");
    PresidentialPardonForm form3("bocal");

    //should throw too low grade exception
    std::cout << "test1\n";
    form1.beSigned(b3);

    //should be ok
    std::cout << "test2\n";
    form1.beSigned(b2);
    //should be ok (it is signed)
    std::cout << "test3\n";
    form1.execute(b1);
    std::cout << "test4\n";
    //should throw too low grade exception
    form1.execute(b3);

    //should throw form is not signed exception
    std::cout << "test5\n";
    form3.execute(b3);
    //should be ok
    std::cout << "test6\n";
    form3.beSigned(b1);
    //should be ok
    std::cout << "test7\n";
    form3.execute(b1);
    //should throw too low grade exception
    std::cout << "test8\n";
    form2.beSigned(b3);
    //should be ok
    std::cout << "test9\n";
    form2.beSigned(b2);
    //should throw too low grade exception
    std::cout << "test10\n";
    form2.execute(b3);
    //should be ok
    std::cout << "test11\n";
    form2.execute(b1); //result failed
    //should be ok
    std::cout << "test12\n";
    form2.execute(b1); //res success
}